#include "ThreadPool_test.hh"
#include "gtest/gtest.h"

TEST_F(ThreadPoolTest,TestAll) {
    const int nThreads = 8;
    ThreadPool pool(nThreads);
    pool.GetIDs();

    for(int i = 0; i < nThreads; ++i) {
        for(int j = 0; j < nThreads; ++j) {
            if(i == j) continue;
            EXPECT_NE(pool.GetID(i),pool.GetID(j));
        }
    }

    std::vector<std::shared_future<void>> futures;
    for(int i = 0; i < 64; ++i) {
        futures.emplace_back(pool.enqueue([this,i] {
            for(int j = 0; j < i; ++j) {
            }
        }));
    }
    WaitAll(futures);
}
