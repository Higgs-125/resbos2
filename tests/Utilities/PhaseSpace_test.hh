#ifndef PHASESPACE_TEST_HH
#define PHASESPACE_TEST_HH

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "ResBos/PhaseSpace.hh"
#include "Random_test.hh"

class MockPhaseSpace : public PhaseSpace {
    public:
        // Mock Phase Space generators
        MOCK_METHOD2(PhaseGen,double(double*,std::vector<FourVector>&));
        MOCK_METHOD4(PhaseSpace3,double(double*,double&, double&,std::vector<FourVector>&));
        MOCK_CONST_METHOD6(x2Phase,double(double*,double,double,double,FourVector&,FourVector&));
        MOCK_METHOD4(Wresph,double(double*,double,std::vector<double>&,std::vector<FourVector>&));

        // Mock Setters
        MOCK_METHOD1(SetECM,void(double));
        MOCK_METHOD1(SetNPrimaryPart,void(int));
        MOCK_METHOD1(SetIMonte,void(int));
        MOCK_METHOD1(SetIQt,void(int));
        MOCK_METHOD1(SetMass,void(std::vector<double>));
        MOCK_METHOD2(SetQRange,void(double,double));
        MOCK_METHOD2(SetQtRange,void(double,double));
};

using ::testing::TestWithParam;

class PhaseSpaceTest : public ::testing::TestWithParam<int> {
    public:
//        ~PhaseSpaceTest() {delete rand;}

        // Set-up work for each test
        virtual void SetUp() {
            rand = new MockRandom(); 
        }

        // Clean-up work for each test
        virtual void TearDown() {
            delete rand;
        }
    
    protected:
        std::vector<FourVector> particles; 
        MockRandom *rand;
};

#endif
