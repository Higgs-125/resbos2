#ifndef GRID3D_TEST_HH
#define GRID3D_TEST_HH

#include "gmock/gmock.h"
#include "ResBos/Grid3D.hh"

double TestFunc(const double& x, const double& y, const double& z) {
    const int n = 10;
    return pow(x-n/2.0,2)+pow(y-n/2.0,2)+pow(z-n/2.0,2);
}

namespace Utility {
class MockGrid3D : public Grid3D {
    public:
        // Mock Add point
        MOCK_METHOD4(AddPoint,void(unsigned int, unsigned int, unsigned int, double));
        MOCK_METHOD4(AddPoint,void(double, double, double, double));
       
        // Mock Finders
        MOCK_CONST_METHOD1(FindX,std::vector<double>::const_iterator(double));
        MOCK_CONST_METHOD1(FindY,std::vector<double>::const_iterator(double));
        MOCK_CONST_METHOD1(FindZ,std::vector<double>::const_iterator(double));
        MOCK_CONST_METHOD3(FindPoint, double(unsigned int, unsigned int, unsigned int));
        MOCK_CONST_METHOD3(FindPoint, double(double, double, double));
        MOCK_CONST_METHOD3(FindPointLocation, unsigned int(double, double, double));

        // Mock Getters
        MOCK_CONST_METHOD1(GetX, double(int));
        MOCK_CONST_METHOD1(GetY, double(int));
        MOCK_CONST_METHOD1(GetZ, double(int));
        MOCK_CONST_METHOD0(GetXDim, unsigned int());
        MOCK_CONST_METHOD0(GetYDim, unsigned int());
        MOCK_CONST_METHOD0(GetZDim, unsigned int());

        // Mock Interpolation
        MOCK_CONST_METHOD6(Interpolate, double(double,double,double,int,int,int));
};
}

class Grid3DTest : public ::testing::Test {
    protected:
        // Set-up work for each test
        virtual void SetUp() {
            test = new Utility::Grid3D(x,y,z);
            for(auto xVal : x) {
                for(auto yVal : y) {
                    for(auto zVal : z) {
                        test -> AddPoint(xVal,yVal,zVal,TestFunc(xVal,yVal,zVal));
                    }
                }
            }
        }

        virtual void TearDown() {
            delete test;
        }

        const std::vector<double> x{1,2,3,4,5,6,7,8,9,10};
        const std::vector<double> y{1,2,3,4,5,6,7,8,9,10};
        const std::vector<double> z{1,2,3,4,5,6,7,8,9,10};
        Utility::Grid3D *test;
        
};

#endif
