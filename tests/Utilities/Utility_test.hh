#ifndef UTILITY_TEST_HH
#define UTILITY_TEST_HH

#include "gmock/gmock.h"
#include "ResBos/Utility.hh"

namespace Utility {

class MockIntegrate : public Integrate {
    public:
        // Mock functions
        MOCK_METHOD3(Integral, double(double, double, double));
        MOCK_METHOD1(SetFunction, void(std::function<double(double)>));
        MOCK_METHOD5(AdaptiveIntegrate, double(double,double,double,double,int));
        MOCK_METHOD3(GaussKronrodInt, double(double,double,double&));
        MOCK_METHOD3(NewtonCotesEPInt, double(double,double,double&));
        MOCK_METHOD3(TrapezoidNEInt, double(double,double,double&));
        MOCK_METHOD3(NewtonCotesInt, double(double,double,double&));
        MOCK_METHOD3(DEQuad, double(double,double,double&));
        MOCK_METHOD6(Adzint, double(const double, const double, const double, 
                                    const double, int,  int));
};

}

class IntegrateTest : public ::testing::TestWithParam<double> {
    protected:
        // Set-up work for each test
        virtual void SetUp() {}
        static void SetUpTestSuite() {
            ogata2 = new Utility::OgataQuad2();
        }

        // Clean-up work for each test
        virtual void TearDown() {}
        static void TearDownTestSuite() {
            delete ogata2;
            ogata2 = nullptr;
        }

        static Utility::OgataQuad2 *ogata2;
};

Utility::OgataQuad2* IntegrateTest::ogata2 = nullptr;

class UtilityFuncTest : public ::testing::Test {
    protected:
        // Set-up work for each test
        virtual void SetUp() {}

        // Clean-up work for each test
        virtual void TearDown() {}
};

#endif

