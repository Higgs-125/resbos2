#include "Utility_test.hh"
#include "gtest/gtest.h"

double testFunc1(double x) {
    return x*x+1;
}

double testFunc2(double x) {
    return sin(1.0/x);
}

double testFunc3(double x) {
    return exp(-x*x);
}

double testFunc4(double x) {
    return 1.0/(1+x*x);
}

double testFunc5(double x) {
    return 1.0/(2+cos(x));
}

double testOscFunc(double x) {
    return Utility::BesJ0(x);
}

double rfunc(double r, double a=1) {
    return r*exp(-pow(a*r,2)/2);
}

double kfunc(double k, double a=1) {
    return exp(-pow(k/a,2)/2)/pow(a,2);
}
    
const std::vector<double> besselZero = {0,2.4048256,5.5200781,8.6537279,11.7915344,
    14.9309177,18.0710640,21.2116366,24.3524715,27.4934791,30.6346065,33.7758202,
    36.9170984,40.0584258,43.1997917,46.3411884,49.4826099,52.6240518,55.7655108,
    58.9069839,62.0484692,65.1899648,68.3314693,69.9022245,73.6145006,77.7560256,
    80.8975559,84.0390908,87.1806298,90.3221726,93.4637188,96.6052680,99.7468199,
    102.888374,106.029931,109.171490,112.313050,115.454613,118.596177,121.737742,
    124.879309,128.020877,131.162446,134.304017,137.445588,140.587160,143.728734,
    146.870308,150.011883,153.153458,156.295034,159.436611,162.578189,165.719767,
    168.861345,172.002924,175.144504,178.286084,181.427665,184.569246,187.710827};

TEST_F(IntegrateTest, DoubleExponentialInt1) {
    std::function<double(double)> testFunc = testFunc1;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.DEIntegrate(0,4,1E-8,1E-16)-76.0/3.0),1E-8);
}

TEST_F(IntegrateTest, DoubleExponentialInt2) {
    std::function<double(double)> testFunc = testFunc2;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.DEIntegrate(0.1,2*M_PI,1E-8,1E-16)-2.2717166014),1E-8);
}

TEST_F(IntegrateTest, DoubleExponentialInt3) {
    std::function<double(double)> testFunc = testFunc3;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.DEIntegrate(0,1,1E-8,1E-16)-0.746824132812427),1E-8);
}

TEST_F(IntegrateTest, DoubleExponentialInt4) {
    std::function<double(double)> testFunc = testFunc4;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.DEIntegrate(0,1,1E-16,1E-16)-M_PI_4),1E-8);
}

TEST_F(IntegrateTest, DoubleExponentialInt5) {
    std::function<double(double)> testFunc = testFunc5;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.DEIntegrate(0,M_PI,1E-16,1E-16)-M_PI/sqrt(3.0)),1E-8);
}

TEST_F(IntegrateTest, GaussKronrodInt1) {
    std::function<double(double)> testFunc = testFunc1;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,4,1E-16,1E-8,0)-76.0/3.0),1E-8);
}

/* This is hard for this technique to get right, so do not consider
TEST_F(IntegrateTest, GaussKronrodInt2) {
    std::function<double(double)> testFunc = testFunc2;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0.1,2*M_PI,1E-16,1E-8,0)-2.2717166014),1E-8);
}*/

TEST_F(IntegrateTest, GaussKronrodInt3) {
    std::function<double(double)> testFunc = testFunc3;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,1,1E-16,1E-8,0)-0.746824132812427),1E-8);
}

TEST_F(IntegrateTest, GaussKronrodInt4) {
    std::function<double(double)> testFunc = testFunc4;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,1,1E-16,1E-8,0)-M_PI/4.0),1E-8);
}

TEST_F(IntegrateTest, GaussKronrodInt5) {
    std::function<double(double)> testFunc = testFunc5;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,M_PI,1E-16,1E-8,0)-M_PI/sqrt(3.0)),1E-8);
}

TEST_F(IntegrateTest, NewtonCotesEPInt1) {
    std::function<double(double)> testFunc = testFunc1;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,4,1E-16,1E-8,1)-76.0/3.0),1E-8);
}

/* This is hard for this technique to get right, so do not consider
TEST_F(IntegrateTest, NewtonCotesEPInt2) {
    std::function<double(double)> testFunc = testFunc2;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0.01,2*M_PI,1E-16,1E-8,1)-2.26268575),1E-8);
}*/

// Can only get it to 1E-4 for some reason, need to investigate at some point
TEST_F(IntegrateTest, NewtonCotesEPInt3) {
    std::function<double(double)> testFunc = testFunc3;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,1,1E-16,1E-8,1)-0.7468241328),1E-4);
}

// Can only get it to 1E-5 for some reason, need to investigate at some point
TEST_F(IntegrateTest, NewtonCotesEPInt4) {
    std::function<double(double)> testFunc = testFunc4;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,1,1E-16,1E-8,1)-M_PI/4.0),1E-5);
}

// Can only get it to 1E-4 for some reason, need to investigate at some point
TEST_F(IntegrateTest, NewtonCotesEPInt5) {
    std::function<double(double)> testFunc = testFunc5;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,M_PI,1E-16,1E-8,1)-M_PI/sqrt(3.0)),1E-4);
}

TEST_F(IntegrateTest, NewtonCotesInt1) {
    std::function<double(double)> testFunc = testFunc1;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,4,1E-16,1E-8,2)-76.0/3.0),1E-8);
}

/* This is hard for this technique to get right, so do not consider
TEST_F(IntegrateTest, NewtonCotesInt2) {
    std::function<double(double)> testFunc = testFunc2;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0.01,2*M_PI,1E-8,1E-8,2)-2.26268575),1E-8);
}*/

// Can only get it to 1E-5 for some reason, need to investigate at some point
TEST_F(IntegrateTest, NewtonCotesInt3) {
    std::function<double(double)> testFunc = testFunc3;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,1,1E-16,1E-8,2)-0.7468241328),1E-5);
}

// Can only get it to 1E-5 for some reason, need to investigate at some point
TEST_F(IntegrateTest, NewtonCotesInt4) {
    std::function<double(double)> testFunc = testFunc4;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,1,1E-16,1E-8,2)-M_PI/4.0),1E-5);
}

/* Does not seem to work for this function, need to investigate at some point 
TEST_F(IntegrateTest, NewtonCotesInt5) {
    std::function<double(double)> testFunc = testFunc5;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,M_PI,1E-16,1E-8,2)-M_PI/sqrt(3.0)),1E-8);
}*/

/* Slow convergence, works best for functions with endpoint singularities
TEST_F(IntegrateTest, TrapezoidNEInt1) {
    std::function<double(double)> testFunc = testFunc1;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,4,1E-16,1E-8,3)-76.0/3.0),1E-8);
}*/

TEST_F(IntegrateTest, TrapezoidNEInt2) {
    std::function<double(double)> testFunc = testFunc2;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0.01,2*M_PI,1E-16,1E-8,3)-2.26268575),1E-8);
}

/* Slow convergence, works best for functions with endpoint singularities
TEST_F(IntegrateTest, TrapezoidNEInt3) {
    std::function<double(double)> testFunc = testFunc3;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,1,1E-16,1E-8,3)-0.7468241328),1E-8);
}

TEST_F(IntegrateTest, TrapezoidNEInt4) {
    std::function<double(double)> testFunc = testFunc4;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,1,1E-16,1E-8,3)-M_PI/4.0),1E-8);
}

TEST_F(IntegrateTest, TrapezoidNEInt5) {
    std::function<double(double)> testFunc = testFunc5;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,M_PI,1E-16,1E-8,3)-M_PI/sqrt(3.0)),1E-8);
}*/

TEST_F(IntegrateTest, DEQuad1) {
    std::function<double(double)> testFunc = testFunc1;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,4,1E-16,1E-8,3)-76.0/3.0),1E-8);
}

TEST_F(IntegrateTest, DEQuad2) {
    std::function<double(double)> testFunc = testFunc2;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0.01,2*M_PI,1E-8,1E-8,3)-2.26268575),1E-8);
}

TEST_F(IntegrateTest, DEQuad3) {
    std::function<double(double)> testFunc = testFunc3;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,1,1E-16,1E-8,3)-0.7468241328),1E-8);
}

TEST_F(IntegrateTest, DEQuad4) {
    std::function<double(double)> testFunc = testFunc4;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,1,1E-16,1E-8,3)-M_PI/4.0),1E-8);
}

TEST_F(IntegrateTest, DEQuad5) {
    std::function<double(double)> testFunc = testFunc5;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.AdaptiveIntegrate(0,M_PI,1E-16,1E-8,3)-M_PI/sqrt(3.0)),1E-8);
}

TEST_F(IntegrateTest, Adzint1) {
    std::function<double(double)> testFunc = testFunc1;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.Adzint(0,4,1E-16,1E-8,1,1)-76.0/3.0),1E-8);
}

TEST_F(IntegrateTest, Adzint1a) {
    std::function<double(double)> testFunc = testFunc1;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.Adzint(0,4,1E-16,1E-8,0,0)-76.0/3.0),1E-8);
}

TEST_F(IntegrateTest, Adzint1b) {
    std::function<double(double)> testFunc = testFunc1;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.Adzint(0,4,1E-16,1E-8,2,2)-76.0/3.0),1E-8);
}

TEST_F(IntegrateTest, Adzint2) {
    std::function<double(double)> testFunc = testFunc2;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.Adzint(0.01,2*M_PI,1E-8,1E-8,1,1)-2.26268575),1E-8);
}

TEST_F(IntegrateTest, Adzint3) {
    std::function<double(double)> testFunc = testFunc3;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.Adzint(0,1,1E-16,1E-8,1,1)-0.7468241328),1E-8);
}

TEST_F(IntegrateTest, Adzint4) {
    std::function<double(double)> testFunc = testFunc4;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.Adzint(0,1,1E-16,1E-8,1,1)-M_PI/4.0),1E-8);
}

TEST_F(IntegrateTest, Adzint5) {
    std::function<double(double)> testFunc = testFunc5;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.Adzint(0,M_PI,1E-16,1E-8,1,1)-M_PI/sqrt(3.0)),1E-8);
}

TEST_F(IntegrateTest, OscIntegrate) {
    std::function<double(double)> testFunc = testOscFunc;
    Utility::Integrate integrate(testFunc);
    EXPECT_LT(fabs(integrate.OscIntegrate(besselZero,1E-8)-1.0),1E-8);
}

TEST_F(IntegrateTest, OgataIntegrate) {
    double err;
    Utility::OgataQuad ogata(5E-3);
    const auto func = [=](double const& r) -> double{return Utility::BesJ0(r);};
    double result = ogata.Transform(func,err);
    EXPECT_NEAR(result,1.0,std::max(10*err,1E-16));
}

TEST_P(IntegrateTest, OscIntegrate2) {
    double k = GetParam();
    std::function<double(double)> testFunc = [=] (double const &r) 
        -> double{return rfunc(r/k)*Utility::BesJ0(r);};
    Utility::Integrate integrate(testFunc);
    EXPECT_NEAR(integrate.OscIntegrate(besselZero,1E-8)/k,kfunc(k),1E-5);
}

TEST_P(IntegrateTest, OgataIntegrate2) {
    double k = GetParam();
    double err;
    std::function<double(double)> func = [=] (double const &r) 
        -> double{return rfunc(r/k)*Utility::BesJ0(r);};
    Utility::OgataQuad ogata(1E-3);
    double result = ogata.Transform(func,err); 
    std::cout.precision(6);
    std::cout << std::scientific << kfunc(k) << " " << result/k << std::endl;
    EXPECT_NEAR(result/k,kfunc(k),std::max(err,1E-15));
}

TEST_P(IntegrateTest, Ogata2Integrate2) {
    double k = GetParam();
    std::function<double(double)> func = [&] (double const &b) 
        -> double{ return b*exp(-b); };
    auto qfunc = [&](const double& qt) { return pow(1+qt*qt,-1.5); };
    double result = ogata2 -> FBT(func, k, k, 1E-6, 1E-8); 
    std::cout.precision(6);
    std::cout << std::scientific << qfunc(k) << " " << result << std::endl;
    EXPECT_NEAR(result,qfunc(k),1E-6);
}

INSTANTIATE_TEST_SUITE_P(IntegrateTestKCheck,IntegrateTest,::testing::Range(0.2,10.0,0.2));

TEST(UtilityTest, DivDif) {
     
}

TEST(UtilityTest, Zeta) {   
    EXPECT_LT(fabs(Utility::zeta(2)-M_PI*M_PI/6.0),1E-8);
    EXPECT_LT(fabs(Utility::zeta(4)-pow(M_PI,4)/90.0),1E-8);
    EXPECT_LT(fabs(Utility::zeta(12)-691*pow(M_PI,12)/638512875.0),1E-8);
    EXPECT_LT(fabs(Utility::zeta(14)-2*pow(M_PI,14)/18243225.0),1E-8);
}

TEST(UtilityTest, PolyLog) {
    EXPECT_EQ(Utility::xLi(-1,0.5),2);
    EXPECT_EQ(Utility::xLi(0,0.5),1);
    EXPECT_LT(fabs(Utility::xLi(2,0.5)-0.5822405264650127),1E-8);
    EXPECT_LT(fabs(Utility::xLi(7,0.5)-0.5020145633247085),1E-8);
    EXPECT_LT(fabs(Utility::xLi(15,0.5)-0.5000076381652628),1E-8);
    EXPECT_LT(fabs(Utility::xLi(3,0.995)-1.193896987191249),1E-8);
    EXPECT_LT(fabs(Utility::xLi(3,0.5)-0.5372131936080403),1E-8);

    double y = 0.5;
    EXPECT_EQ(Utility::xLi(3,1.0-y),Utility::xLi(3,y));
    EXPECT_EQ(1-y,y);
}

TEST(UtilityTest, Polint) {
    std::function<double(double)> func = [](double x) {return x*x*x+3.0;};
    std::vector<double> x_ = {0,1,2,3,4,5,6,7,8,9};
    std::vector<double> y_;
    for(auto x : x_) {
        y_.push_back(func(x));
    }

    for(int i = 0; i < 100; ++i) {
        double x = 9.0/100.0*i;
        EXPECT_LT(fabs(Utility::Polint(x_,y_,4,x)-func(x)),1E-5);
    }
}
