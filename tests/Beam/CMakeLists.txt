# Build tests for the Beam functions
add_executable(testBeam test.cc Pdf_test.cc HoppetInterface_test.cc Beam_test.cc Convolution_test.cc)
add_dependencies(testBeam GTest)
include_directories(${RESBOS_SOURCE_DIR}/include)
include_directories(${LHAPDF_INCLUDE_DIR})
include_directories(${Hoppet_INCLUDE_DIR})
include_directories(${FFTW_INCLUDE_DIR})
if(USE-GZIP)
target_link_libraries(testBeam LINK_PUBLIC 
    gtestd 
    gtest_maind 
    gmockd 
    ResbosUtilities 
    ResbosBeam 
    ResbosUser
    Resbos 
    ${LHAPDF_LIBRARY} 
    ${Hoppet_LIBRARY} 
    gfortran 
    Threads::Threads
    dl
    ${Backtrace_LIBRARIES}
    gzstream)
else(USE-GZIP)
target_link_libraries(testBeam LINK_PUBLIC 
    gtestd 
    gtest_maind 
    gmockd 
    ResbosUtilities 
    ResbosBeam 
    ResbosUser
    Resbos 
    ${LHAPDF_LIBRARY} 
    ${Hoppet_LIBRARY} 
    ${Backtrace_LIBRARIES}
    gfortran 
    Threads::Threads
    dl)
endif(USE-GZIP)
add_test(testBeam testBeam)
