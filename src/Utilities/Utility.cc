#include "ResBos/Utility.hh"

#include <iomanip>
#include <mutex>

#include "ResBos/loguru.hpp"

typedef std::function<double(double,double,double&)> Integrator;

namespace Utility {

    Integrate::Integrate(std::function<double(double)> func) {
        f = func;
        Integrators.push_back(Integrator(std::bind(&Integrate::GaussKronrodInt,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3)));
        Integrators.push_back(Integrator(std::bind(&Integrate::NewtonCotesEPInt,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3)));
        Integrators.push_back(Integrator(std::bind(&Integrate::NewtonCotesInt,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3)));
//        Integrators.push_back(Integrator(std::bind(&Integrate::TrapezoidNEInt,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3)));
        Integrators.push_back(Integrator(std::bind(&Integrate::DEQuad,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3)));
        count = 0;
        scaleFac = 0;
    }

    Integrate::Integrate(std::vector<std::function<double(double)>> func) {
        fVec = func;
        size = fVec.size();
        Integrators.push_back(Integrator(std::bind(&Integrate::GaussKronrodInt,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3)));
        Integrators.push_back(Integrator(std::bind(&Integrate::NewtonCotesEPInt,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3)));
        Integrators.push_back(Integrator(std::bind(&Integrate::NewtonCotesInt,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3)));
//        Integrators.push_back(Integrator(std::bind(&Integrate::TrapezoidNEInt,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3)));
        Integrators.push_back(Integrator(std::bind(&Integrate::DEQuad,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3)));
        count = 0;
        scaleFac = 0;
    }

    std::mutex intMutex;

    double Integrate::GetFunctionValue(double x) {
        std::map<double,double>::iterator it = functionGrid.find(x);
        double fx;
        if(it == functionGrid.end()) {
            fx = f(x);
            functionGrid[x] = fx;
        } else {
            fx = it->second;
        }
        return fx*pow(x,scaleFac);
    }

    std::vector<double> Integrate::GetFunctionValueVec(double x) {
        std::vector<double> fx;
        for(auto func : fVec) fx.push_back(func(x));
        return fx;
    }

/* This is garbage
    double Integrate::CoarseIntegrate(double a, double b){
        double fa = GetFunctionValue(a), fb = GetFunctionValue(b);
        return 0.5*(b-a)*(fa + fb);
    }

    double Integrate::FineIntegrate(double a, double b){
        double c = (b+a)/2.0;
        double fa = GetFunctionValue(a), fb = GetFunctionValue(b), fc = GetFunctionValue(c);
        return 0.25*(b-a)*(fa+2.0*fc+fb);
    }

    double Integrate::Integral(double a, double b, double tolerance) {
        double coarse = CoarseIntegrate(a,b);
        double fine = FineIntegrate(a,b);

        if(count >= minLevel && fabs(fine - coarse) <= 3.0 * tolerance) {
            return fine;
        } else if(count >= maxLevel) {
            return fine;
        } else {
            ++count;
            return Integral(a, (b+a)/2.0,tolerance/2.0) + Integral((b+a)/2.0, b, tolerance/2.0);
        }
    }*/

    std::mutex integrateMutex;

    double Integrate::AdaptiveIntegrate(double a, double b, double aerr, double rerr,int mode) {
        double err;
        double integral = Integrators[mode](a, b, err);
        error.resize(maxSubintervals);
        lowerList.resize(maxSubintervals);
        upperList.resize(maxSubintervals);
        integralList.resize(maxSubintervals);

        error[0] = err;
        lowerList[0] = a;
        upperList[0] = b;
        integralList[0] = integral;

        double errorBound = std::max(aerr, rerr*std::abs(integral)); 

        DLOG_F(3,"Iteration 1 (lower, upper, value, error): %e, %e, %e, %e",
                lowerList[0],upperList[0],integral,error[0]);

        if(err < errorBound) return integral;

        double area = integral;
        double errorSum = err;
        double errorMax = err;

        int maxError = 0;
        int nSubintervals;

        for(nSubintervals = 2; nSubintervals <= maxSubintervals; ++nSubintervals) {
            double lower1 = lowerList[maxError];
            double upper2 = upperList[maxError];

            double upper1 = (lower1+upper2)*0.5;
            double lower2 = upper1;

            double error1, error2;

            double area1 = Integrators[mode](lower1, upper1, error1);
            double area2 = Integrators[mode](lower2, upper2, error2);

            DLOG_F(3,"Iteration %d (lower1, upper1, value1, error1): %e, %e, %e, %e",
                    nSubintervals,lower1,upper1,area1,error1);
            DLOG_F(3,"Iteration %d (lower2, upper2, value2, error2): %e, %e, %e, %e",
                    nSubintervals,lower2,upper2,area2,error2);

            double area12 = area1 + area2;
            double error12 = error1 + error2;
            errorSum += error12 - errorMax;
            area += area12 - integralList[maxError];

            integralList[maxError] = area1;
            integralList[nSubintervals-1] = area2;

            if(error2 > error1) {
                lowerList[nSubintervals-1] = lower1;
                lowerList[maxError] = lower2;
                upperList[nSubintervals-1] = upper1;
                integralList[maxError] = area2;
                integralList[nSubintervals-1] = area1;
                error[maxError] = error2;
                error[nSubintervals-1] = error1;
            } else {
                lowerList[nSubintervals-1] = lower2;
                upperList[maxError] = upper1;
                upperList[nSubintervals-1] = upper2;
                error[maxError] = error1;
                error[nSubintervals-1] = error2;
            }

            maxError = std::distance(error.begin(), std::max_element(error.begin(),error.end())); 

            errorBound = std::max(aerr, rerr*std::abs(area));

            if(errorSum <= errorBound) break;
        }

        integral = 0;

        for(int k = 0; k < nSubintervals; ++k) {
            integral += integralList[k];
        }
        
        return integral;
    }

    // Newton Cotes integration (3 point rule with end points)
    double Integrate::NewtonCotesEPInt(double a, double b, double &err) {
        double center = (a+b)*0.5;
        //double fCenter = GetFunctionValue(center);

        double resultNC3point = GetFunctionValue(a) + 4*GetFunctionValue(center) + GetFunctionValue(b) ;
        resultNC3point *= (b-a)/6 ;
        double resultNC5point = GetFunctionValue(a) + 4*GetFunctionValue(a/2+center/2) + 2*GetFunctionValue(center) + 4*GetFunctionValue(center/2+b/2) + GetFunctionValue(b) ;    
        resultNC5point *= (b-a)/12 ;

        err = fabs((resultNC5point - resultNC3point)/15) ;

        return resultNC5point ;
    }


    /* Newton Cotes integration (2 point rule without end points)
     * Convergence is way to slow to be any use
    double Integrate::TrapezoidNEInt(double a, double b, double &err) {
        double step = (b-a)/3.0;

        double resultTrap2point = (b-a)/2.0 * ( GetFunctionValue(a+step) + GetFunctionValue(a+2*step) ); 
        double resultTrap4point = (b-a)/4.0 * ( GetFunctionValue(a+step/2.0) + GetFunctionValue(a+step) + GetFunctionValue(a+3*step/2.0) + GetFunctionValue(a+2*step) );

        err = fabs(( resultTrap4point - resultTrap2point )/3.0);

        return resultTrap4point ;
    }*/

    // Newton Cotes 4 point rule without end points
    double Integrate::NewtonCotesInt(double a, double b, double &err) {
        double step = (b-a)/5 ;

        double resultNC4point = (b-a)/24 * ( 11*GetFunctionValue(a+step) + GetFunctionValue(a+2*step) + GetFunctionValue(a+3*step) + 11*GetFunctionValue(a+4*step) ) ;
        double resultNC8point = (b-a)/48 * ( 11*GetFunctionValue(a+step/2) + GetFunctionValue(a+step) + GetFunctionValue(a+3*step/2) + 11*GetFunctionValue(a+2*step) + 11*GetFunctionValue(a+3*step) + GetFunctionValue(a+7*step/2) + GetFunctionValue(a+4*step) + 11*GetFunctionValue(b-step/2) ) ;


        err = fabs(( resultNC8point - resultNC4point )/15) ;

        return resultNC8point ;

    }

    // Double exponential transform quadurature
    double Integrate::DEQuad(double a, double b, double&) {
        double h, ss, z, exz, hcos, hsin, s, dxdz, x1, x2, w;

        const int n = 1000;
        h = 5.0/n;

        ss = 0.5*GetFunctionValue((a+b)/2.0);
        for(int k = -n; k < 0; ++k) {
            z = h*k;
            exz = exp(z);
            hcos = exz+1.0/exz;
            hsin = exz-1.0/exz;
            s = exp(M_PI_4*hsin);
            w = s + 1.0/s;
            dxdz = hcos/(w*w);
            x1 = (b*s+a/s)/w;
            x2 = (a*s+b/s)/w;
            if(x1 != a && x1 != b) ss += GetFunctionValue(x1)*dxdz;
            if(x2 != a && x2 != b) ss += GetFunctionValue(x2)*dxdz;
        }

        return 2.0*(b-a)*M_PI_4*h*ss;

    }

    // Double exponential integration (more efficient and with adaptive built-in)
    double Integrate::DEIntegrate(const double x1, const double x2, 
                                  const double ceps1, const double ceps2) {
        static const int izx = 5;
        double ax = 0.5*(x2-x1);
        double bx = 0.5*(x2+x1);
        double sum = 0.0, s1 = 0.0, s2 = 0.0, s3 = 0.0;
        double fm1 = 0.0, fm2 = 0.0, err = 0.0, h = 1.0;
        double xx1, xx2, t1, t2, tw1, tw2;

        int phases = _phases+1, k1, k2, iz1, iz2;
        int *ip = new int[phases];
        ip[0] = 1;
        for(int i = 1; i < phases; ++i) ip[i] = 2*ip[i-1];

        int evals = 0;
        for(long int k = 1; k <= _phases; ++k) {
            h *= 0.5;
            s3 = s2;
            s2 = s1;
            fm1 = 0.0;
            fm2 = 0.0;
            k1 = ip[_phases-k];
            k2 = ip[_phases-k+1];

            iz1 = iz2 = 0;

            //Evaluate function at level k in x, avoiding unnecessary computation.
            for(long int i = 0; i < _size; i += k1) {
                if(i % k2 != 0 || k == 1) {
                    double xt1 = 1.0 - table.abscissa[i];
                    xx1 = bx - ax*xt1;
                    xx2 = bx + ax*xt1;
                    bool log1 = xx1 > x1 && iz1 < izx;
                    bool log2 = xx2 < x2 && iz2 < izx;

                    if(!log1 && !log2) break;

                    if(log1) {
                        t1 = GetFunctionValue(xx1);
                        evals++;
                        tw1 = t1*table.weights[i];
                        if(std::abs(tw1) < ceps1) iz1++;
                        else iz1 = 0;
                    } else {
                        t1 = 0.0;
                        tw1 = 0.0;
                    }

                    if(i > 0 && log2) {
                        t2 = GetFunctionValue(xx2);
                        evals++;
                        tw2 = t2*table.weights[i];
                        if(std::abs(tw2) < ceps1) iz2++;
                        else iz2 = 0;
                    } else {
                        t2 = 0.0;
                        tw2 = 0.0;
                    }

                    sum += tw1 + tw2;
                    tw1 = std::abs(tw1);
                    tw2 = std::abs(tw2);
                    t1 = std::abs(t1);
                    t2 = std::abs(t2);

                    fm1 = std::max(fm1, tw1);
                    fm1 = std::max(fm1, tw2);
                    fm2 = std::max(fm2, t1);
                    fm2 = std::max(fm2, t2);
                }
            }

            s1 = ax*h*sum;
            double eps1 = fm1*ceps1;
            double eps2 = fm2*ceps2;
            double d1 = log10(std::abs(s1 - s2));
            double d2 = log10(std::abs(s1 - s3));
            double d3 = log10(eps1) - 1;
            double d4 = log10(eps2) - 1;

            if(k <= 2)
                err = 1.0;
            else if(d1 == -16.0)
                err = 0.0;
            else {
                double val1 = d1*d1/d2;
                double val2 = 2*d1;
                double max = std::max(val1, val2);
                max = std::max(max,d3);
                max = std::max(max,d4);
                err = pow(10.0, int(std::min(0.0,max)));
            }

            DLOG_F(3,"Iteration %d (value, error): %e, %e",k,s1,err);
            DLOG_F(3,"Number of evaluations = %d", evals);

            if(k > 3 && err < eps1) {
                delete ip;
                return s1;
            }

            if (k >= 3 && err < eps2) {
                delete ip;
                return s1;
            }
        }

        delete ip;
        return s1;
    }

    // Double exponential integration for vectors (more efficient and with adaptive built-in)
    std::vector<double> Integrate::DEIntegrateVec(const double x1, const double x2, 
                                  const double ceps1, const double ceps2) {
        static const int izx = 5;
        double ax = 0.5*(x2-x1);
        double bx = 0.5*(x2+x1);
        double err = 0.0, h = 1.0;
        double xx1, xx2;
        std::vector<double> t1(size), t2(size), tw1(size), tw2(size);
        std::vector<double> sum(size), s1(size), s2(size), s3(size);
        std::vector<double> fm1(size), fm2(size);

        int phases = _phases+1, k1, k2, iz1, iz2;
        int *ip = new int[phases];
        ip[0] = 1;
        for(int i = 1; i < phases; ++i) ip[i] = 2*ip[i-1];

        int evals = 0;
        for(long int k = 1; k <= _phases; ++k) {
            h *= 0.5;
            for(size_t j = 0; j < size; ++j) {
                s3[j] = s2[j];
                s2[j] = s1[j];
                fm1[j] = 0.0;
                fm2[j] = 0.0;
            }
            k1 = ip[_phases-k];
            k2 = ip[_phases-k+1];

            iz1 = iz2 = 0;

            //Evaluate function at level k in x, avoiding unnecessary computation.
            for(long int i = 0; i < _size; i += k1) {
                if(i % k2 != 0 || k == 1) {
                    double xt1 = 1.0 - table.abscissa[i];
                    xx1 = bx - ax*xt1;
                    xx2 = bx + ax*xt1;
                    bool log1 = xx1 > x1 && iz1 < izx;
                    bool log2 = xx2 < x2 && iz2 < izx;

                    if(!log1 && !log2) break;

                    if(log1) {
                        t1 = GetFunctionValueVec(xx1);
                        evals++;
                        for(size_t j = 0; j < size; ++j) tw1[j] = t1[j]*table.weights[i];
                        if(std::abs(tw1[0]) < ceps1) iz1++;
                        else iz1 = 0;
                    } else {
                        for(size_t j = 0; j < size; ++j) {
                            t1[j] = 0.0;
                            tw1[j] = 0.0;
                        }
                    }

                    if(i > 0 && log2) {
                        t2 = GetFunctionValueVec(xx2);
                        evals++;
                        for(size_t j = 0; j < size; ++j) tw2[j] = t2[j]*table.weights[i];
                        if(std::abs(tw2[0]) < ceps1) iz2++;
                        else iz2 = 0;
                    } else {
                        for(size_t j = 0; j < size; ++j) {
                            t2[j] = 0.0;
                            tw2[j] = 0.0;
                        }
                    }

                    for(size_t j = 0; j < size; ++j) {
                        sum[j] += tw1[j] + tw2[j];
                        tw1[j] = std::abs(tw1[j]);
                        tw2[j] = std::abs(tw2[j]);
                        t1[j] = std::abs(t1[j]);
                        t2[j] = std::abs(t2[j]);

                        fm1[j] = std::max(fm1[j], tw1[j]);
                        fm1[j] = std::max(fm1[j], tw2[j]);
                        fm2[j] = std::max(fm2[j], t1[j]);
                        fm2[j] = std::max(fm2[j], t2[j]);
                    }
                }
            }

            for(size_t j = 0; j < size; ++j) s1[j] = ax*h*sum[j];
            double eps1 = fm1[0]*ceps1;
            double eps2 = fm2[0]*ceps2;
            double d1 = log10(std::abs(s1[0] - s2[0]));
            double d2 = log10(std::abs(s1[0] - s3[0]));
            double d3 = log10(eps1) - 1;
            double d4 = log10(eps2) - 1;

            if(k <= 2)
                err = 1.0;
            else if(d1 == -16.0)
                err = 0.0;
            else {
                double val1 = d1*d1/d2;
                double val2 = 2*d1;
                double max = std::max(val1, val2);
                max = std::max(max,d3);
                max = std::max(max,d4);
                err = pow(10.0, int(std::min(0.0,max)));
            }

            DLOG_F(3,"Iteration %d (value, error): %e, %e",k,s1[0],err);

            if(k > 3 && err < eps1) {
                delete ip;
                return s1;
            }

            if (k >= 3 && err < eps2) {
                delete ip;
                return s1;
            }
        }

        delete ip;
        return s1;
    }


    // Calculation for alternating series proposed by Cohen, Villegas, Zagier (Convergence Acceleration of Alternating Series, Experimental Mathematics, 9:1, 3-12)
    double Integrate::OscIntegrate(std::vector<double> zeros, const double eps) {
        int n = std::ceil(-1.31*log10(eps));
//        int n = 20;
        double d = pow(3.0+sqrt(8),n);
        d = (d+1/d)/2;
        double b = -1, c = -d, s = 0;
        for(int i = 0; i < n; ++i) {
            c = b - c;
#ifndef FITTING
            if(i % 2 == 0)
                s += c*DEIntegrate(zeros[i],zeros[i+1],1E-8,1E-8);
            else 
                s -= c*DEIntegrate(zeros[i],zeros[i+1],1E-8,1E-8);
#else //FITTING
            double err;
            if(i % 2 == 0)
                s += c*GaussKronrodInt(zeros[i],zeros[i+1],err);
            else 
                s -= c*GaussKronrodInt(zeros[i],zeros[i+1],err);
#endif
            b = (i+n)*(i-n)*b/((i+0.5)*(i+1));
        }
        return s/d;
    }

    // Calculation for vectors for alternating series 
    // proposed by Cohen, Villegas, Zagier (Convergence Acceleration of Alternating Series, Experimental Mathematics, 9:1, 3-12)
    std::vector<double> Integrate::OscIntegrateVec(std::vector<double> zeros, const double eps) {
        int n = std::ceil(-1.31*log10(eps));
//        int n = 20;
        double d = pow(3.0+sqrt(8),n);
        d = (d+1/d)/2;
        double b = -1, c = -d;
        std::vector<double> s(size);
        for(int i = 0; i < n; ++i) {
            c = b - c;
            std::vector<double> integral = DEIntegrateVec(zeros[i],zeros[i+1],1E-16,1E-8);
            for(size_t j = 0; j < size; ++j) {
                if(i % 2 == 0)
                    s[j] += c*integral[j]/d;
                else 
                    s[j] -= c*integral[j]/d;
            }
            b = (i+n)*(i-n)*b/((i+0.5)*(i+1));
        }
        return s;
    }

    double Integrate::GaussKronrodInt(double a, double b, double &err) {
        double halfLength = (b-a)*0.5;
        double center = (a+b)*0.5;
        double fCenter = GetFunctionValue(center);

        double resultGauss = GaussWgts[GaussWgts.size()-1]*fCenter;
        double resultKronrod = KonrodWgts[KonrodWgts.size()-1]*fCenter;

        double absIntegral = std::abs(resultKronrod);

        for(size_t j = 1; j < KonrodWgts.size()-GaussWgts.size(); ++j) {
            int jj = 2*j-1;
            double abscissa = halfLength * absc[jj];

            double f1 = GetFunctionValue(center - abscissa);
            double f2 = GetFunctionValue(center + abscissa);
            double fSum = f1+f2;

            resultGauss += GaussWgts[j-1]*fSum;
            resultKronrod += KonrodWgts[jj]*fSum;

            absIntegral += KonrodWgts[jj] * (std::abs(f1) + std::abs(f2));
        }

        for(size_t j = 0; j < GaussWgts.size(); j++) {
            int jj = 2*j;
            double abscissa = halfLength * absc[jj];

            double f1 = GetFunctionValue(center - abscissa);
            double f2 = GetFunctionValue(center + abscissa);
            double fSum = f1+f2;

            resultKronrod += KonrodWgts[jj]*fSum;

            absIntegral += KonrodWgts[jj] * (std::abs(f1) + std::abs(f2));
        }

        double resultMeanKronrod = resultKronrod * 0.5;
        double absDiffIntegral = KonrodWgts[KonrodWgts.size()-1]*(std::abs(fCenter - resultMeanKronrod));
        for(size_t j = 0; j < KonrodWgts.size(); ++j) {
            double abscissa = halfLength * absc[j];
            absDiffIntegral += std::abs(GetFunctionValue(center-abscissa)-resultMeanKronrod)*KonrodWgts[j];
            absDiffIntegral += std::abs(GetFunctionValue(center+abscissa)-resultMeanKronrod)*KonrodWgts[j];
        }

        absIntegral *= std::abs(halfLength);
        absDiffIntegral *= std::abs(halfLength);
        err = std::abs((resultKronrod - resultGauss)*halfLength);

        if(absDiffIntegral != 0 && err != 0) 
            err = absDiffIntegral*std::min(1.0,
                    pow(200*err/absDiffIntegral,1.5));

        if(absIntegral > (std::numeric_limits<double>::min)() / (1E-12 * 50.))
            err = std::max(1E-12*50*absIntegral, err);

        return resultKronrod*halfLength;
    }

    void Integrate::ScaleFunc(int n) {
        scaleFac = n;
    }

    double Integrate::Adzint(const double a, const double b, const double aerr, const double rerr, const int iactaIn, const int iactbIn){
        res = 0;
        ers = 0;
        iacta = iactaIn;
        iactb = iactbIn;
        double sml = 1E-12;
        double target = 0;
        int numold;
        double ddx = b - a;

        fa = 0; fb = 0; ib = 0; ier = 0;

        if(ddx==0) {
            return 0;
        }
        if(ddx<0) {
            throw std::runtime_error("Integration method only works for b < a");
        }

        if(ddx <= sml) {
            return GetFunctionValue(a+ddx/2.0)*ddx;
        }

        numint = 3;
        double dx = ddx / (double)numint;

        for(int i = 0; i < numint; i++) {
            if(i==0) {
                u[0] = a;
                if(iacta == 0) {
                    fu[0] = GetFunctionValue(a);
                } else {
                    fa = GetFunctionValue(a+dx/2.0);
                }
            } else {
                u[i] = v[i-1];
                fu[i] = fv[i-1];
            }
            if(i == numint-1) {
                v[i]=b;
                if(iactb == 0) {
                    fv[i]=GetFunctionValue(b);
                } else {
                    ib = i;
                    fb=GetFunctionValue(b-dx/2.0);
                }
            } else {
                v[i] = a+dx*(i+1);
                fv[i] = GetFunctionValue(v[i]);
            }
            Privcal(i,result[i],err[i]);
        }

        for(int i = 0; i < numint; i++){
            res += result[i];
            ers += err[i];
        }


        double const fac0 = 2.0, facMax=fac0*maxint;
        bool numltmax = true;
        while(ier==0 && ers>target && numltmax) {
            numold = numint;
            double facnum = sqrt((double)numold*fac0);
            do {
                for(int i = 0; i < numold; i++) 
                    if(err[i]*facnum > target) Privspl(i);
                if(facnum > facMax) {numltmax=false;break;}
            }while(numold==numint && (facnum*=fac0));
            target = std::abs(aerr) + std::abs(rerr*res);
        }

        //    if(ier == 1) {
        //        std::clog << "WARNING: maxint reached (target, error): " << target << ", " << ers << std::endl;
        //    }

        return res;
    }

    void Integrate::sglint(int iact, double f1, double f2, double f3, double dx, double *fint, double *ester) {
        double tem, tmp, er;
        double huge = 1E20;
        tem = dx * (4.0*f1+3.0*f2+2.0*f3)/9.0;
        er = dx * (4.0*f1 - 6.0*f2 + 2.0*f3) / 9.0;

        if(iact == 2) {
            double t1 = f2 - f1;
            double t2 = f3 - f2;
            if(t1*t2 <= 0) {
                *fint = tem;
                *ester = er;
                return;
            }
            double t3 = t2 - t1;
            double t12=t1*t1;
            if(std::abs(f3)*huge < t12) {
                *fint = tem;
                *ester = er;
                return;
            }
            double cc = log(t2/t1)/log(2.0);
            if(cc <= 1.0) {
                *fint = tem;
                *ester = er;
                return;
            }
            double bb = t12/t3;
            double aa = (f1*f3 - f2*f2)/t3;
            tmp = dx*(aa+bb*pow(4,cc)/(cc+1));
            er = tem - tmp;
            tem = tmp;
        }

        *fint = tem;
        *ester = er;
    }

    void Integrate::Privcal(int i, double &result, double &err) {
        double tem, dx, w, er;

        dx = v[i] - u[i];
        w = (u[i] + v[i]) / 2.0;

        if(i == 0 && iacta > 0) {
            fw[i] = fa;
            fa = GetFunctionValue(u[i] + dx/4.0);
            sglint(iacta, fa, fw[i], fv[i], dx, &tem, &er);
        } else if(i == ib && iactb > 0) {
            fw[i] = fb;
            fb = GetFunctionValue(v[i] - dx/4.0);
            sglint(iactb, fb, fw[i], fu[i], dx, &tem, &er);
        } else {
            fw[i] = GetFunctionValue(w);
            tem = dx*(fu[i] + 4.0 * fw[i] + fv[i])/6.0;
            er = dx*(fu[i] - 2.0*fw[i]+fv[i])/12.0;
        }
        result = tem;
        err = std::abs(er);
    }

    void Integrate::Privspl(int i) {
        double tiny = 1E-20, oldres, olderr, delres, gooderr, sumerr, fac;

        if(numint >= maxint){
            ier = 1;
            return;
        }

        if(i==ib) ib=numint;
        u[numint]=(u[i]+v[i])/2.0;
        v[numint]=v[i];

        fu[numint]=fw[i];
        fv[numint]=fv[i];

        v[i] = u[numint];
        fv[i] = fu[numint];

        oldres = result[i];
        olderr = err[i];

        Privcal(i,result[i],err[i]);
        Privcal(numint,result[numint],err[numint]);

        delres = result[i] + result[numint] - oldres;
        res += delres;

        gooderr = std::abs(delres);

        ers += gooderr - olderr;
        sumerr = err[i] + err[numint];
        if(sumerr > tiny) fac = gooderr / sumerr;
        else fac = 1.0;

        err[i] *= fac;
        err[numint] *= fac;

        numint++;
    }

    /* No longer needed for now
    double DivDif(double* f, double* a, int nn, double x, int mm){
        double t[20], d[20];
        int n, m, ix, iy, mid, npts, mPlus;
        int ip, l, iSub, i, j;
        int mMax=20;
        if(nn > 2 || mm > 1) {
            n = nn;
            m = std::min(std::min(mm,mMax),n-1);
            mPlus = m+1;
            ix = 0;
            iy = n+1;
            while(iy-ix>1){
                if(a[0] < a[n]) {
                    mid = (ix+iy)/2;
                    if(x>=a[mid-1]) {
                        ix=mid;
                    } else {
                        iy=mid;
                    }
                } else {
                    mid = (ix+iy)/2;
                    if(x<=a[mid-1]) {
                        ix=mid;
                    } else {
                        iy=mid;
                    }
                }
            }
            npts=m+2-m%2;
            ip=0;
            l=0;
            while(ip < npts){
                iSub=ix+l;
                if(!(1<=iSub && iSub<= n)){
                    npts=mPlus;
                } else {
                    ip++;
                    t[ip-1]=a[iSub-1];
                    d[ip-1]=f[iSub-1];
                }
                l=-l;
                if(l>=0) l++;
            }
            bool extra=npts!=mPlus;

            for(l=1; l<m; l++){
                if(extra) {
                    iSub=mPlus-l;
                    d[m+1]=(d[m+1]-d[m-1])/(t[m+1]-t[iSub-1]);
                }
                i=mPlus;
                for(int j=l; j < m; j++) {
                    iSub = i-l;
                    d[i-1]=(d[i-1]-d[i-2])/(t[i-1]-t[iSub-1]);
                    i--;
                }
            }

            double tot = d[mPlus-1];
            if(extra) tot = 0.5*(tot+d[m+1]);
            j=m;
            for(l=1;l<m;l++){
                tot=d[j-1]+(x-t[j-1])*tot;
                j--;
            }
            return tot;
        } else return 0;
    }*/

    // From this function down to the Polint function were inspired from:
    // https://github.com/Expander/polylogarithm/blob/master/src/Li.cpp
    bool is_close(double a, double b, double eps = 1E-10) {
        return std::abs(a - b) < eps;
    }

    bool is_even(long n) { return n % 2 == 0;}

    double fac(double n) {
        double result = 1.0;
        for(long i = 1; i <=n; ++i) result *= i;
        return result;
    }

    double binomial(long n, long k) {
        double result = 1.0;

        if( k > n-k) k = n-k;

        for(long i = 0; i < k; ++i) {
            result *= (n - i);
            result /= (i + 1);
        }

        return result;
    }

    double zeta(double s) {
        static std::map<double,double> cached;
        if(cached.find(s) != cached.end()) return cached[s];
        else {
#if __cpp_lib_math_special_functions >= 201603
            const double result = std::riemann_zeta(s);
            cached[s] = result;
            return result;
#else
            if(s==1) return inf;

            double sum = 0.0, sum_old = 0.0;
            long n = 0;

            if(s >= 12) {
                n = 1;
                do{
                    sum_old = sum;
                    sum += pow(n,-s);
                    n++;
                } while(!is_close(sum_old,sum) &&
                        n < std::numeric_limits<long>::max() - 2);
                cached[s] = sum;
                return sum;
            }

            do{
                sum_old = sum;
                double sub_sum = 0.0;

                for(long k = 0; k <= n; ++k) {
                    const long sgn = is_even(k) ? 1 : -1;
                    sub_sum += binomial(n,k)*sgn*std::pow(k+1,-s);
                }

                sum += sub_sum*std::pow(2.,-(n+1));
                n++;
            } while (!is_close(sum_old, sum) &&
                     n < std::numeric_limits<long>::max() - 2);

            cached[s] = sum/(1.0 - std::pow(2.0,1-s));
            return sum/(1.0 - std::pow(2.0,1-s));
        }
#endif
    }

    double eta(long n) {
        return (1.0 - std::pow(2.,1-n))*zeta(n);
    }

    std::vector<std::array<double,N>> Xn(long p) {
        using TArray = std::array<double,N>;
        std::vector<TArray> xn(p+1);

        {
            TArray ar;
            for(long ni = 0; ni < N; ++ni) {
                ar[ni] = bernoulli[ni];
            }
            xn[0] = ar;
        }

        for(long pi = 1; pi <= p; ++pi) {
            TArray ar;
            for(long ni = 0; ni < N; ++ni) {
                double sum = 0.0;
                for(long k = 0; k <= ni; ++k) {
                    sum += binomial(ni,k)*bernoulli[ni-k]/(k+1)*xn[pi-1][k];
                }
                ar[ni] = sum;
            }
            xn[pi] = ar;
        }

        return xn;
    }

    double xLi_negative(long n, const double x) {
        if(is_close(x,1)) return inf;

        double result = 0.0;
        for(long k = 0; k <= -n; ++k) {
            double sum = 0.0;
            for(long j = 0; j <= k; ++j) {
                const long sgn = is_even(j) ? -1 : 1;
                sum += sgn*binomial(k,j)*std::pow(j+1,-n);
            }

            result += std::pow(-x/(1.-x),k+1)*sum;
        }

        return result;
    }

    double xLi_naive_sum(long n, const double x) {
        double sum = 0, sum_old;
        long k = 0;

        do{
            k++;
            sum_old = sum;
            sum += std::pow(x,k)/std::pow(k,n);
        } while(!is_close(sum, sum_old) &&
                k < std::numeric_limits<long>::max() - 2);

        return sum;
    }

    double H(long n) {
        double sum = 0.0;

        for(long h = 1; h <= n; ++h) sum += 1.0/h;

        return sum;
    }

    double xLi_around_one(long n, const double x) {
        const double mu = log(x);
        double sum=0.0, sum_old=0.0;
        long k = 0;

        do{
            if(k == n-1) {
                k++;
                continue;
            }
            sum_old = sum;
            sum += zeta(n-k)/fac(k)*std::pow(mu,k);
            k++;
        } while(!is_close(sum, sum_old) &&
                k < std::numeric_limits<long>::max() - 2);

        return std::pow(mu,n-1)/fac(n-1)*(H(n-1)-log(-mu))+sum;
    }

    double xLi(long n, const double x) {
        static std::map<std::pair<long,double>,double> cached;
        if(cached.find(std::make_pair(n,x)) != cached.end()) {
            return cached[std::make_pair(n,x)];
        } else {
            if(std::abs(x) - 1 > 1E-16) 
                throw std::runtime_error("Li: x out of range (-1,1) , x=" + std::to_string(x));
            if(n < 0) {
                const double result = xLi_negative(n,x);
                cached[std::make_pair(n,x)] = result;
                return result;
            } else if(n == 0) {
                if(is_close(x,1)) return inf;
                return x/(1.0-x);
            } else if(n == 1) return -log(1-x);
            else if(is_close(x,0)) return 0;
            else if(is_close(x,1)) return zeta(n);
            else if(is_close(x,-1)) return -eta(n);
            else if(n >= 12) return xLi_naive_sum(n,x);
            else if(is_close(x,1,1E-2)) {
                const double result = xLi_around_one(n,x);
                cached[std::make_pair(n,x)] = result;
                return result;
            } else {
                double u = -log(1-x);
                double p = 1., sum = 0;
                const auto xn = Xn(n-2);
                for(long k = 0; k < N; ++k) {
                    p *= u;
                    sum += xn[n-2][k]*p*fac_inv[k];
                }
                cached[std::make_pair(n,x)] = sum;
                return sum;
            }
        }
    }

//    double xLi(int n, double x){
//        int i = 0, NCUT=27;
//        double L=0.0, r=1.0, xt, xln1m;
//        const double c1[7] = {0.75, -0.58333333333333333, 0.45486111111111111111,
//            -0.3680555555555555555555, 0.3073611111111111111111,
//            -0.2630555555555555555,0.2294880243764172};
//        const double c2[7] = {-0.5,0.5,-0.458333333333333333,0.4166666666666666666,
//            -0.3805555555555555555555555,0.35,-0.3241071428571428};
//        static const double zeta3 = zeta(3);
//        double pi2by6 = M_PI*M_PI/6.0;
//
//        if(x == 1) {
//            if(n==3) return zeta3;
//            return zeta(n);
//        } else if(x==-1) {
//            return -(1-pow(2,1-n))*zeta(n);
//        }
//
//        if(std::abs(x) - 1 > 1E-16) {
//            std::cout << "Li: x out of range (-1,1) , x=" << x << std::endl;
//            exit(-1);
//        }
//
//        if(n < 0) {
//            std::cout << "The polilogartihm Li undefined for n=" << n << std::endl;
//            exit(-1);
//        } else if (n == 0) {
//            return x/(1-x);
//        } else if (n == 1) {
//            return -log(1-x);
//        } else if (n == 2) {
//            if (x == 0) {
//                return 0;
//            } else if (x >= -0.5 && x <= 0.5) {
//                while(i  < NCUT){
//                    i++;
//                    r*=x;
//                    L+=r/i/i;
//                }
//                return L;
//            } else if(x>0.5) {
//                xt = 1.0-x;
//                L = pi2by6 - log(x)*log(xt);
//                while(i < NCUT) {
//                    i++;
//                    r*=xt;
//                    L-=r/i/i;
//                }
//                return L;
//            } else if(x<-0.5) {
//                xt=-x/(1-x);
//                L=-0.5*log(1-x)*log(1-x);
//                while(i < NCUT) {
//                    i++;
//                    r*=xt;
//                    L-=r/i/i;
//                }
//                return L;
//            }
//        } else if(n==3 && x >= 0.9) {
//            L = zeta3+pi2by6*log(x);
//            xt=(1-x);
//            xln1m=log(xt);
//
//            for (i = 2; i < 9; i ++) {
//                L += (c1[i-2]+c2[i-2]*xln1m)*pow(xt,i);
//            }
//            return L;
//        } else {
//            while (i < NCUT) {
//                i++;
//                r*=x;
//                L+=r/pow((double)i,double(n));
//            }
//            return L;
//        }
//    }

    double Polint(std::vector<double> x_, std::vector<double> y_, int n, double x) {
        // Function to preform a polynomial interpolation of order n
        int ns=0;
        double dift, dif = std::abs(x-x_[0]);
        std::vector<double> c(n), d(n);
        for(int i = 0; i < n; i++) {
            if((dift=fabs(x-x_[i])) < dif) {
                ns=i;
                dif=dift;
            }
            c[i] = y_[i];
            d[i] = y_[i];
        }
        double y = y_[ns--];
        double ho,hp,w,den;
        for(int m = 0; m < n-1; m++) {
            for(int i = 0; i <  n-m-1; i++) {
                ho = x_[i]-x;
                hp = x_[i+m+1]-x;
                w = c[i+1]-d[i];
                if((den=ho-hp) == 0) throw std::runtime_error("Polint: Error in interpolation routine");
                den = w/den;
                d[i] = hp*den;
                c[i] = ho*den;
            }
            y += (2*ns+1 < (n-m-1) ? c[ns+1] : d[ns--]);
        }
        return y;
    }

    /* Not needed anymore for now. Also needs to be rewritten since it doesn't agree
     * with Mathematica
    double LambertW(const double z) {
        // Function from Keith Briggs (keithbriggs.info/software/LambertW1.c)

        const double eps = 1E-10, em1 = exp(-1);
        double p=1.0,e,t,w,l1,l2;
        if(z < -em1) throw std::runtime_error("LambertW: Invalid z argument");
        // Initial approximation 
        if(z<1E-6) { //Series about -1/e
            p=-sqrt(2.0*(2.7182818284590452353602874713526625*z+1.0));
            w=-1.0+p*(1.0+p*(-0.333333333333333333333+p*0.1527777777777777777777));
        } else { // Series about 0
            l1 = log(-z);
            l2 = log(-l1);
            w = l1 - l2 + l2/l1;
        }
        if(std::fabs(p)<1E-4) return w;
        for(int i = 0; i < 10; i++) { //Halley iteration
            e=exp(w);
            t=w*e-z;
            p=w+1.0;
            t/=e*p-0.5*(p+1.0)*t/p;
            w-=t;
            if(std::fabs(t)<eps*(1.0+std::fabs(w))) return w; // relative - absolute error
        }
        std::cout << std::scientific;
        std::cout << "LambertW: Warning: Did not reach desired precision: " << z << "  " << w << "  " << std::fabs(t) << "  " << eps*(1.0+std::fabs(w)) << std::endl;
        std::cout << std::fixed;
        return w;
    }*/

}
