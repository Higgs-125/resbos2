#include "ResBos/Grid2D.hh"
#include "ResBos/Utility.hh"

#include "ResBos/loguru.hpp"

template class std::vector<double>;

namespace Utility {

    void Grid2D::AddPoint(unsigned int i, unsigned int j, double value) {
        grid[i+xSize*j] = value; 
    }

    void Grid2D::AddPoint(double x, double y, double value) {
        unsigned int point = FindPointLocation(x,y);
        grid[point] = value;
    }

    std::vector<double>::const_iterator Grid2D::FindX(double x) const {
        return std::lower_bound(x_.begin(), x_.end(), x);
    }

    std::vector<double>::const_iterator Grid2D::FindY(double y) const {
        return std::lower_bound(y_.begin(), y_.end(), y);
    }

    double Grid2D::FindPoint(unsigned int i, unsigned int j) const {
        return grid[i+xSize*j];
    }

    double Grid2D::FindPoint(double x, double y) const {
        return grid[FindPointLocation(x,y)];
    }

    unsigned int Grid2D::FindPointLocation(double x, double y) const {
        unsigned int i = std::distance(x_.begin(),FindX(x));
        unsigned int j = std::distance(y_.begin(),FindY(y));

        return i+xSize*j;
    }


    double Grid2D::Interpolate(double x, double y, int xOrder, int yOrder){
        // Find location of point
        //unsigned location = FindPointLocation(x,y);

        // Obtain a vector of the nearest xOrder*yOrder points and ensure it doesn't go over a boundary
        // Begin with the x direction
        // 1) Check if it is within xOrder/2 of an edge (if the order is odd, add additional point to the end)
        // 2) Fill with the surrounding xOrder elements
        std::vector<double>::iterator itX = std::lower_bound(x_.begin(), x_.end(), x);
        while(std::distance(x_.begin(),itX) < xOrder/2) itX++;
        while(std::distance(itX,x_.end()) < xOrder/2+xOrder%2) itX--;
        std::vector<double> xInterp(itX-xOrder/2,itX+xOrder/2+xOrder%2); 

        // Repeat for y direction
        std::vector<double>::iterator itY = std::lower_bound(y_.begin(), y_.end(), y);
        while(std::distance(y_.begin(),itY) < yOrder/2) itY++;
        while(std::distance(itY,y_.end()) < yOrder/2+yOrder%2) itY--;
        std::vector<double> yInterp(itY-yOrder/2,itY+yOrder/2+yOrder%2); 

        std::vector<double> tmp(yOrder), tmp2(xOrder);

        for(int i = 0; i < xOrder; i++) {
            for(int j = 0; j < yOrder; j++) {
                tmp[j] = grid[FindPointLocation(xInterp[i],yInterp[j])];
            }
            tmp2[i] = Polint(yInterp,tmp,yOrder,y);
        }
        double result = Polint(xInterp,tmp2,xOrder,x);
        DLOG_F(3,"Interpolation result: x = %f y = %f f(x,y) = %f",x,y,result);
        return result;
    }

    void Grid2D::PrintGrid() {
        for(unsigned int i = 0; i < xSize; i++) {
            for(unsigned int j = 0; j < ySize; j++) {
                std::cout << x_[i] << "\t" << y_[j] << "\t" << FindPoint(i,j) << std::endl;
            }
        }
    }

}
