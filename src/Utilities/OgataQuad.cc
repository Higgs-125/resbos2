#include <functional>

#include "ResBos/Utility.hh"
#include "ResBos/loguru.hpp"
#include "ResBos/BesselZeros.hh"
#include "ResBos/Brent.hh"

namespace Utility {
    inline double psi(const double& t) {
        return t*tanh(M_PI*sinh(t)/2);
    }

    inline double psip(const double& t) {
        return (M_PI*t*cosh(t)+sinh(M_PI*sinh(t)))/(1+cosh(M_PI*sinh(t)));
    }

    OgataQuad::OgataQuad() {
        h = 1E-2;
        init();
    }

    void OgataQuad::init() {
        for(auto zero : besselZero) {
            const double z = zero/M_PI;
            weights.push_back(2/pow(j1(zero),2)/M_PI/M_PI/z);
        }

        N = size_t(M_PI/h) < nBesselJ ? size_t(M_PI/h) : nBesselJ;
        LOG_IF_F(WARNING, N == nBesselJ, "OgataQuad: Number of zeros required greater than tabled zeros.");
    }
            
    void OgataQuad::SetStep(double _h) {
        h = _h;
        N = size_t(M_PI/h) < nBesselJ ? size_t(M_PI/h) : nBesselJ;
        LOG_IF_F(WARNING, N == nBesselJ, "OgataQuad: Number of zeros required greater than tabled zeros.");
    }

    double OgataQuad::GetParams(std::function<double(const double&)> func, double start, const double& decrement, const double& rtol, const double& atol, const size_t& maxiter) {
        if(start >= 1) throw std::runtime_error("OgataQuad: Invalid starting value for h, must be less than 1");

        size_t iter = 0;
        double err;
        SetStep(start);
        double res = Transform(func,err,rtol,atol);
        double res2 = 2*res + 10;

        while(std::abs((res - res2)/res) > rtol && iter < maxiter) {
            iter++;
            start /= decrement;
            SetStep(start);
            res2 = res;
            res = Transform(func,err,rtol,atol);
        }

        if(iter == maxiter) 
            throw std::runtime_error("OgataQuad: Max iteration reached while checking for convergence");

        return start * decrement;
    }

    double OgataQuad::Transform(std::function<double(const double&)> func, double& err, const double& rerr, const double& aerr) const {
        double result = 0;
        size_t i;
        for(i = 0; i < N; ++i) {
            double x = M_PI*psi(h*besselZero[i]/M_PI)/h;
            LOG_F(9,"x[%d] = %e",i,x);
            err = M_PI * weights[i] * func(x) * psip(h*besselZero[i]/M_PI);
            result += err;

            LOG_F(7,"Relative Error = %e, Absolute Error = %e",std::abs(err/result),std::abs(err));
            if(std::abs(err/result) < rerr && std::abs(err) < aerr) break;

            if(i > 5 && result == 0) {
                DLOG_F(INFO,"OgataQuad: Result seems to be zero");
                err = 0;
                return 0;
            }
        }
       
        err = std::abs(err);
        LOG_IF_F(WARNING,i==N,"OgataQuad: Precision of %e reached, requested rerr = %e, aerr = %e",err,rerr,aerr);

        DLOG_F(INFO,"OgataQuad: Result = %e +/- %e", result, err);
        DLOG_F(INFO,"Number of function evals: %d", i);

        return result;
    }

    std::vector<double> OgataQuad::Transform(std::function<std::vector<double>(const double&)> func, std::vector<double>& err, const double& rerr, const double& aerr) const {
        const size_t fsize = func(1.0).size();
        std::vector<double> result(fsize);
        size_t i;
        double max_rerr, max_aerr;
        for(i = 0; i < N; ++i) {
            double x = M_PI*psi(h*besselZero[i]/M_PI)/h;
            LOG_F(9,"x[%d] = %e",i,x);
            std::vector<double> tmp = func(x);
            for(size_t j = 0; j < fsize; ++j) {
                err[j] = M_PI * weights[i] * tmp[j] * psip(h*besselZero[i]/M_PI);
                result[j] += err[j];
            }

            max_rerr = *std::max_element(err.begin(),err.end());
            std::vector<double> aerr_(fsize);
            std::transform(err.begin(),err.end(),result.begin(),std::back_inserter(aerr_),std::divides<double>());
            max_aerr = *std::max_element(aerr_.begin(),aerr_.end());
            LOG_F(7,"Relative Error = %e, Absolute Error = %e",std::abs(max_rerr),std::abs(max_aerr));
            if(std::abs(max_rerr) < rerr && std::abs(max_aerr) < aerr) break;
        }
       
        LOG_IF_F(WARNING,i==N,"OgataQuad: Precision of %e reached, requested rerr = %e, aerr = %e",max_aerr,rerr,aerr);

        DLOG_F(INFO,"OgataQuad: Result = %e +/- %e", result[0], err[0]);
        DLOG_F(INFO,"Number of function evals: %d", i);

        return result;
    }

    constexpr std::array<double, OgataQuad2::maxN> OgataQuad2::jnZeros;

    void OgataQuad2::init() {
        for(size_t i = 0; i < maxN; ++i) {
            xi[i] = jnZeros[i]/M_PI;
            double jp1 = BesJ1(M_PI*xi[i]);
            w[i] = BesY0(M_PI*xi[i]) / jp1;
        }
    }

    double OgataQuad2::OgataT(const func& f, const double &h, const double &rerr,
                              const double &aerr) const noexcept {

        double result = 0;
        size_t i;
        double err;
        for(i = 0; i < int(M_PI/h); ++i) {
            double knot = M_PI/h*GetPsi(h*xi[i]);
            double jnu = BesJ0(knot);
            double psip = GetPsiP(h*xi[i]);
            if(psip != psip) psip = 1.0;
            err = M_PI*w[i]*jnu*psip*f(knot);
            result += err;
            //std::cout << i << " " << err << " " << result << " " << exp(-1.0/h) << std::endl;

            if(std::abs(err/result) < rerr && std::abs(err) < aerr) break;

            if(i > 5 && result == 0) {
                DLOG_F(INFO,"OgataQuad: Result seems to be zero");
                err = 0;
                return 0;
            }
        }
      
        err = std::abs(err);
        LOG_IF_F(WARNING, i==N,
                 "OgataQuad: Precision of %e reached, requested rerr = %e, aerr = %e",
                 err, rerr, aerr);

        DLOG_F(INFO,"OgataQuad: Result = %e +/- %e", result, err);
        DLOG_F(INFO,"Number of function evals: %d", i);

        return result;
    }

    std::vector<double> OgataQuad2::OgataT(const funcVec& f, const double &h, const double &rerr,
                                           const double &aerr) const noexcept {
        static const size_t fsize = f(1.0).size();
        static std::vector<double> err(fsize);
        static std::vector<double> aerr_(fsize);

        std::vector<double> result(fsize, 0.0);
        size_t i;
        const size_t N = M_PI/h;
        double max_rerr, max_aerr;
        for(i = 0; i < N; ++i) {
            double knot = N*GetPsi(h*xi[i]);
            double psip = GetPsiP(h*xi[i]);
            if(psip != psip) psip = 1.0;
            double wgt = M_PI*w[i]*BesJ0(knot)*psip;

            // Calculate results for the whole vector
            err = f(knot);
            for(size_t j = 0; j < fsize; ++j) {
                err[j] *= wgt;
                result[j] += err[j];
            }

            // Find max relative and absolute errors to use
            max_rerr = *std::max_element(err.begin(), err.end(), AbsCompare);
            std::transform(err.begin(), err.end(), result.begin(), 
                           aerr_.begin(), std::divides<double>());
            max_aerr = *std::max_element(aerr_.begin(),aerr_.end(), AbsCompare);
            if(std::abs(max_rerr) < rerr && std::abs(max_aerr) < aerr) break;
        }

        max_aerr = std::abs(max_aerr);
        max_rerr = std::abs(max_rerr);
        LOG_IF_F(WARNING, i == N && h != hDefault,
                 "OgataQuad %i %e: Precision of %e and %e reached, requested rerr = %e, aerr = %e",
                 i, h, max_rerr, max_aerr, rerr, aerr);
        return result;
    }

    double OgataQuad2::OgataU(const func &f, const double &h, const double &rerr,
                              const double &aerr) const noexcept {
        
        double result = 0;
        size_t i;
        double err;
        for(i = 0; i < maxN; ++i) {
            double knot = xi[i]*h;
            err = h*w[i]*f(knot)*BesJ0(knot);
            result += err;

            if(std::abs(err/result) < rerr && std::abs(err) < aerr) break;

            if(i > 5 && result == 0) {
                DLOG_F(INFO,"OgataQuad: Result seems to be zero");
                err = 0;
                return 0;
            }
        }
        
        err = std::abs(err);
        LOG_IF_F(WARNING, i==N,
                 "OgataQuad: Precision of %e reached, requested rerr = %e, aerr = %e",
                 err, rerr, aerr);

        DLOG_F(INFO,"OgataQuad: Result = %e +/- %e", result, err);
        DLOG_F(INFO,"Number of function evals: %d", i);

        return result;
    }

    double OgataQuad2::GetHu(const func &f, const double &q, const std::pair<double, double> &Q) const noexcept {
        auto h = [&](const double &x) { return -std::abs(x*f(x/q)); };
        Brent brent(h);
        double b = brent.Minimize(Q.first, Q.second);
        if(b > Q.second) return Q.second/jnZeros[0];
        if(b < Q.first) return Q.first/jnZeros[0];
        return b/jnZeros[0];
    }

    double OgataQuad2::GetHu(const funcVec &f, const double &q, const std::pair<double, double> &Q) const noexcept {
        auto h = [&](const double &x) { 
            auto tmp = f(x/q); 
            double max = *std::max_element(tmp.begin(), tmp.end());
            return -std::abs(x*max);
        };
        Brent brent(h);
        double b = brent.Minimize(Q.first, Q.second);
        if(b > Q.second) return Q.second/jnZeros[0];
        if(b < Q.first) return Q.first/jnZeros[0];
        return b/jnZeros[0];
    }

    double OgataQuad2::GetHt(const double& hu) const noexcept {
        auto f = [&](const double &h) { 
            return hu - M_PI*tanh(M_PI/2*sinh(h*jnZeros[15]/M_PI)); 
        };
        Brent brent(f);
        try {
            return brent.CalcRoot(2*hu/M_PI/jnZeros[1], 2*hu/M_PI/jnZeros[50]);
        } catch (std::exception &e) {
            return hu;
        }
    }

    double OgataQuad2::FBTU(const func &g, const double &q, const std::pair<double, double> &Q,
                            const double &rerr, const double &aerr) const noexcept {
        double hu = GetHu(g, q, Q);
        auto f = [&](const double &x) { return g(x/q)/q; };
        return OgataU(f, hu, rerr, aerr);
    }

    double OgataQuad2::FBT(const func &g, const double &q, const std::pair<double, double> &Q,
                           const double &rerr, const double &aerr) const noexcept {
        double hu = GetHu(g, q, Q);
        double ht = GetHt(hu);
        if(ht > 0.01) ht = hDefault;
        auto f = [&](const double &x) { return g(x/q)/q; };
        return OgataT(f, ht, rerr, aerr);
    }

    std::vector<double> OgataQuad2::FBT(const funcVec &g, const double &q,
                                        const std::pair<double, double> &Q,
                                        const double &rerr, const double &aerr) const noexcept {
        double hu = GetHu(g, q, Q);
        double ht = GetHt(hu);
        if(ht > 0.01) ht = hDefault;
        auto f = [&](const double &x) { 
            auto results = g(x/q);
            for(auto& result : results) result /= q;
            return results;
        };
        return OgataT(f, ht, rerr, aerr);
    }
}
