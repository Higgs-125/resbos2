#include "ResBos/Grid3D.hh"
#include "ResBos/Utility.hh"

#include "ResBos/loguru.hpp"

template class std::vector<double>;

namespace Utility {

    void Grid3D::AddPoint(unsigned int i, unsigned int j, unsigned int k, double value) {
        grid[i+xSize*j+xSize*ySize*k] = value; 
    }

    void Grid3D::AddPoint(double x, double y, double z, double value) {
        unsigned int point = FindPointLocation(x,y,z);
        grid[point] = value;
    }

    std::vector<double>::const_iterator Grid3D::FindX(double x) const {
        return std::lower_bound(x_.begin(), x_.end(), x);
    }

    std::vector<double>::const_iterator Grid3D::FindY(double y) const {
        return std::lower_bound(y_.begin(), y_.end(), y);
    }

    std::vector<double>::const_iterator Grid3D::FindZ(double z) const {
        return std::lower_bound(z_.begin(), z_.end(), z);
    }

    double Grid3D::FindPoint(unsigned int i, unsigned int j, unsigned int k) const {
        return grid[i+xSize*j+xSize*ySize*k];
    }

    double Grid3D::FindPoint(double x, double y, double z) const {
        return grid[FindPointLocation(x,y,z)];
    }

    unsigned int Grid3D::FindPointLocation(double x, double y, double z) const {
        unsigned int i = std::distance(x_.begin(),FindX(x));
        unsigned int j = std::distance(y_.begin(),FindY(y));
        unsigned int k = std::distance(z_.begin(),FindZ(z));

        return i+xSize*j+xSize*ySize*k;
    }


    double Grid3D::Interpolate(double x, double y, double z, int xOrder, int yOrder, int zOrder){
        // Obtain a vector of the nearest xOrder*yOrder*zOrder points and ensure it doesn't go over a boundary
        // Begin with the x direction
        // 1) Check if it is within xOrder/2 of an edge (if the order is odd, add additional point to the end)
        // 2) Fill with the surrounding xOrder elements
        std::vector<double>::iterator itX = std::lower_bound(x_.begin(), x_.end(), x);
        while(std::distance(x_.begin(),itX) < xOrder/2) ++itX;
        while(std::distance(itX,x_.end()) < xOrder/2+xOrder%2) --itX;
        std::vector<double> xInterp(itX-xOrder/2,itX+xOrder/2+xOrder%2); 

        // Repeat for y direction
        std::vector<double>::iterator itY = std::lower_bound(y_.begin(), y_.end(), y);
        while(std::distance(y_.begin(),itY) < yOrder/2) ++itY;
        while(std::distance(itY,y_.end()) < yOrder/2+yOrder%2) --itY;
        std::vector<double> yInterp(itY-yOrder/2,itY+yOrder/2+yOrder%2); 

        // Repeat for z direction
        std::vector<double>::iterator itZ = std::lower_bound(z_.begin(), z_.end(), z);
        while(std::distance(z_.begin(),itZ) < zOrder/2) ++itZ;
        while(std::distance(itZ,z_.end()) < zOrder/2+zOrder%2) --itZ;
        std::vector<double> zInterp(itZ-zOrder/2,itZ+zOrder/2+zOrder%2); 

        std::vector<double> tmp(yOrder), tmp2(xOrder), tmp3(zOrder);

        for(int i = 0; i < xOrder; i++) {
            for(int j = 0; j < yOrder; j++) {
                for(int k = 0; k < zOrder; k++) {
                    tmp3[k] = grid[FindPointLocation(xInterp[i],yInterp[j],zInterp[k])];
                }
                tmp[j] = Polint(zInterp,tmp3,zOrder,z);
            }
            tmp2[i] = Polint(yInterp,tmp,yOrder,y);
        }
        double result = Polint(xInterp,tmp2,xOrder,x);
        DLOG_F(3,"Interpolation result: x = %f y = %f z = %f f(x,y,z) = %f",x,y,z,result);
        return result;
    }

    void Grid3D::PrintGrid() {
        for(unsigned int i = 0; i < xSize; i++) {
            for(unsigned int k = 0; k < zSize; k++) {
                for(unsigned int j = 0; j < ySize; j++) {
                    std::cout << x_[i] << "\t" << y_[j] << "\t" << z_[k] << "\t" << FindPoint(i,j,k) << std::endl;
                }
            }
        }
    }

}

