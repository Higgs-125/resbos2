// ***************************************************************
// This file was created using the bat-project script
// for project NonPert.
// bat-project is part of Bayesian Analysis Toolkit (BAT).
// BAT can be downloaded from http://mpp.mpg.de/bat
// ***************************************************************

#include <BAT/BCLog.h>
#include <BAT/BCAux.h>

#include "NonPertFit/NonPertFit.hh"
#include "ResBos/loguru.hpp"

int main()
{
    // Startup logger
    loguru::g_stderr_verbosity = loguru::Verbosity_WARNING;

    // open log file
    BCLog::OpenLog("log.txt", BCLog::detail, BCLog::detail);

    // create new NonPertFit object
    NonPertFit m("SIYY-g");

    // set precision
    m.SetPrecision(BCEngineMCMC::kMedium);

    BCLog::OutSummary("Test model created");

    // Add Data
    BCDataSet data;
    data.ReadDataFromFileTxt("ptData.txt",13);
    data.Fix(0); // Fix data type
    data.Fix(1); // Fix QMin
    data.Fix(2); // Fix QMax
    data.Fix(3); // Fix Pt
    data.Fix(6); // Fix dumSys
    data.Fix(7); // Fix dumUnCorr
    data.Fix(9); // Fix <y>
    data.Fix(10); // Fix <Q>
    data.Fix(11); // Fix yk
    data.Fix(12); // Fix efficiency

    // Assign data to the model
    m.SetDataSet(&data);

    // Testing
//    double prediction2;
//    auto t0 = std::chrono::system_clock::now();
//    for(size_t i = 0; i < m.GetNDataPoints(); ++i) {
//        // get data point
//        std::vector<double>& x = m.GetDataSet()->GetDataPoint(i).GetValues();
//
//        int type = x[0];
//        double Qmin = x[1];
//        double Qmax = x[2];
//        double pT = x[3];
//        
//        if(type == 4) {
//            std::cout << pT << " " << m.experiments[0] -> GetPrediction(Qmin,Qmax,pT,1.0) << " " << x[4] << std::endl;
//        }
//    }
//    auto t1 = std::chrono::system_clock::now();
//    std::chrono::duration<double> elapsed = t1 - t0;
//    std::cout << elapsed.count() << std::endl;
//    throw;

    // Change the maximum number of prerun steps from 10,000 to 20,000
    // m.SetNIterationsPreRunMax(10);

    //////////////////////////////
    // perform your analysis here

    // Normalize the posterior by integrating it over the full parameter space
    // m.Normalize();

    // Write Markov Chain to a ROOT file as a TTree
    // m.WriteMarkovChain(m.GetSafeName() + "_mcmc.root", "RECREATE");

    // run MCMC, marginalizing posterior
    m.MarginalizeAll(BCIntegrate::kMargMetropolis);

    // run mode finding; by default using Minuit
    m.SetOptimizationMethod(BCIntegrate::kOptMinuit);
    m.FindMode(m.GetBestFitParameters());

    // draw all marginalized distributions into a PDF file
    m.PrintAllMarginalized(m.GetSafeName() + "_plots.pdf");

    // print summary plots
    m.PrintParameterPlot(m.GetSafeName() + "_parameters.pdf");
    m.PrintCorrelationPlot(m.GetSafeName() + "_correlation.pdf");
    m.PrintCorrelationMatrix(m.GetSafeName() + "_correlationMatrix.pdf");
    m.PrintKnowledgeUpdatePlots(m.GetSafeName() + "_update.pdf");

    // print results of the analysis into a text file
    m.PrintSummary();

    // Check data one last time
    std::vector<double> pars = m.GetBestFitParameters();
    std::vector<double> g{pars[0],pars[1],pars[2]};
    double prediction = 0;
    double N4 = pars[3];
    double N5 = pars[4];
    double N10 = pars[5];
    double N11 = pars[6];
    double N12 = pars[7];
    double N13 = pars[8];
    double N14 = pars[9];
    double N15 = pars[10];
    double N16 = pars[11];
    for(auto experiment : m.experiments) experiment -> SetGees(g);
    for(size_t i = 0; i < m.GetNDataPoints(); ++i) {
        // get data point
        std::vector<double>& x = m.GetDataSet()->GetDataPoint(i).GetValues();

        int type = x[0];
        double Qmin = x[1];
        double Qmax = x[2];
        double pT = x[3];
        
        if(type == 4) {
            prediction = m.experiments[0] -> GetPrediction(Qmin,Qmax,pT,N4);
        } else if(type == 5) {
            prediction = m.experiments[1] -> GetPrediction(Qmin,Qmax,pT,N5);
        } else if(type == 10) {
            prediction = m.experiments[2] -> GetPrediction(Qmin,Qmax,pT,N10);
        } else if(type == 11) {
            prediction = m.experiments[3] -> GetPrediction(Qmin,Qmax,pT,N11);
        } else if(type == 12) {
            prediction = m.experiments[4] -> GetPrediction(Qmin,Qmax,pT,N12);
        } else if(type == 13) {
            prediction = m.experiments[5] -> GetPrediction(Qmin,Qmax,pT,N13);
        } else if(type == 14) {
            prediction = m.experiments[6] -> GetPrediction(Qmin,Qmax,pT,N14);
        } else if(type == 16) {
            prediction = m.experiments[7] -> GetPrediction(Qmin,Qmax,pT,N15);
        } else if(type == 17) {
            prediction = m.experiments[8] -> GetPrediction(Qmin,Qmax,pT,N16);
        } else continue;
        std::cout << type << " " << prediction << " " << x[4] << " " << x[5] << std::endl;
    }

    // close log file
    BCLog::OutSummary("Exiting");
    BCLog::CloseLog();

    return 0;
}
