// ***************************************************************
// This file was created using the bat-project script.
// bat-project is part of Bayesian Analysis Toolkit (BAT).
// BAT can be downloaded from http://mpp.mpg.de/bat
// ***************************************************************

#include <BAT/BCGaussianPrior.h>
#include <BAT/BCMath.h>
#include "NonPertFit/NonPertFit.hh"
#include "ResBos/Calculation.hh"
#include "ResBos/Convolution.hh"
#include "ResBos/Grid3D.hh"
#include "ResBos/Grid2D.hh"
#include "ResBos/HoppetInterface.hh"
#include "ResBos/Pdf.hh"
#include "ResBos/Process.hh"
#include "ResBos/PhaseSpace.hh"
#include "ResBos/ResBos.hh"
#include "ResBos/Settings.hh"
#include "ResBos/Utility.hh"

#include <iostream>

double pyalem(double q2){
// Returns the running electromagnetic coupling alpha
//PN Stolen shamelessly from ResBos 
//
//...Calculate real part of photon vacuum polarization.
//...For leptons simplify by using asymptotic (q^2 >> m^2) expressions.
//   For hadrons use parametrization of H. Burkhardt et al.
//   See R. Kleiss et al, CERN 89-08, vol. 3, pp. 129-131.
//csb___see also Phys.Lett.B356:398-403,1995

  double pi, alpha0, rpigg, aempi;  
  pi = 3.141592654;
  alpha0=1./137.04;
  aempi= alpha0/(3.*pi);
  
  if (q2 < 2e-6)
    rpigg =0.0;
  else if(q2 < 0.09) 
    rpigg = aempi*(13.4916 + log(q2)) + 0.00835*log(1.+q2);
  else if(q2 < 9.) 
    rpigg=aempi*(16.3200+2.0*log(q2))+ 0.00238*log(1.0+3.927*q2);
  else if(q2 < 1e4) 
    rpigg=aempi*(13.4955+3.0*log(q2))+0.00165+ 0.00299*log(1.0+q2);
  else
    rpigg=aempi*(13.4955+3.0*log(q2))+ 0.00221 + 0.00293*log(1.0+q2);

//...Calculate running alpha_em.
  return alpha0/(1.0-rpigg);
}//pyalem ->

// ---------------------------------------------------------
NonPertFit::NonPertFit(const std::string& name)
    : BCModel(name)
{
    // Define parameters here in the constructor.
    // Also define their priors, if using built-in priors.
    // For example:
    AddParameter("g1", 0, 1, "g_{1}", "");
    GetParameters().Back().SetPriorConstant();
    AddParameter("g2", 0, 1, "g_{2}", "");
    GetParameters().Back().SetPriorConstant();
    AddParameter("g3", 0, 1, "g_{3}", "");
    GetParameters().Back().SetPriorConstant();
    
    // Add in nuisance parameters for the normalization of each experiment
//    AddParameter("N0",0,5,"ATLAS 7TeV Norm","");
//    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N0err));
//    AddParameter("N1",0,5,"ATLAS 7TeV(shape) Norm","");
//    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N1err));
//    AddParameter("N2",0,5,"ATLAS 8TeV Norm","");
//    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N2err));
//    AddParameter("N3",0,5,"ATLAS 8TeV(shape) Norm","");
//    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N3err));
    AddParameter("N4",0.5,1.5,"CDF1 Norm","");
    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N4err));
    AddParameter("N5",0.5,1.5,"CDF2 Norm","");
    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N5err));
//    AddParameter("N6",0,5,"CMS 7TeV Norm","");
//    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N6err));
//    AddParameter("N7",0,5,"CMS 7TeV(shape) Norm","");
//    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N7err));
//    AddParameter("N8",0,5,"CMS 8TeV Norm","");
//    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N8err));
//    AddParameter("N9",0,5,"CMS 8TeV(shape) Norm","");
//    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N9err));
    AddParameter("N10",0.5,1.5,"D01 Norm","");
    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N10err));
    AddParameter("N11",0.5,1.5,"D02 Norm","");
    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N11err));
    AddParameter("N12",0,3,"E288200 Norm","");
    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N12err));
    AddParameter("N13",0,3,"E288300 Norm","");
    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N13err));
    AddParameter("N14",0,3,"E288400 Norm","");
    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N14err));
    AddParameter("N15",0,3,"E605 Norm","");
    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N15err));
    AddParameter("N16",0,3,"R209 Norm","");
    GetParameters().Back().SetPrior(new BCGaussianPrior(1,N16err));

    InitResBos();

    // Define observables here, too. For example:
    //AddObservable("qt_dist", 0, 100, "#mu^{2}", "[GeV^{2}]");
    
    count = 0;
}

// ---------------------------------------------------------
NonPertFit::~NonPertFit()
{
    // destructor
}

// ---------------------------------------------------------
double NonPertFit::LogLikelihood(const std::vector<double>& pars)
{

    static bool first;

    // return the log of the conditional probability p(data|pars).
    // This is where you define your model.
    // BCMath contains many functions you will find helpful.
    std::vector<double> g{pars[0],pars[1],pars[2]};

//    double N0 = 1.0+N0err*pars[4];
//    double N1 = 1.0+N1err*pars[5];
//    double N2 = 1.0+N2err*pars[6];
//    double N3 = 1.0+N3err*pars[7];
    double N4 = pars[3];
    double N5 = pars[4];
//    double N6 = 1.0+N6err*pars[10];
//    double N7 = 1.0+N7err*pars[11];
//    double N8 = 1.0+N8err*pars[12];
//    double N9 = 1.0+N9err*pars[13];
    double N10 = pars[5];
    double N11 = pars[6];
    double N12 = pars[7];
    double N13 = pars[8];
    double N14 = pars[9];
    double N15 = pars[10];
    double N16 = pars[11];

//    std::vector<double> Norms{N4,N5,N10,N11,N12,N13,N14,N16,N17};

//    resbos -> SetNonPertCoefficients(g);

    double logl = 0;
    double prediction = 0;
    for(auto experiment : experiments) experiment -> SetGees(g);
#pragma omp parallel for
    for(size_t i = 0; i < GetNDataPoints(); ++i) {
        // get data point
        std::vector<double>& x = GetDataSet()->GetDataPoint(i).GetValues();

        int type = x[0];
        double Qmin = x[1];
        double Qmax = x[2];
        double pT = x[3];

        if(type == 4) {
            prediction = experiments[0] -> GetPrediction(Qmin,Qmax,pT,N4);
        } else if(type == 5) {
            prediction = experiments[1] -> GetPrediction(Qmin,Qmax,pT,N5);
        } else if(type == 10) {
            prediction = experiments[2] -> GetPrediction(Qmin,Qmax,pT,N10);
        } else if(type == 11) {
            prediction = experiments[3] -> GetPrediction(Qmin,Qmax,pT,N11);
        } else if(type == 12) {
            prediction = experiments[4] -> GetPrediction(Qmin,Qmax,pT,N12);
        } else if(type == 13) {
            prediction = experiments[5] -> GetPrediction(Qmin,Qmax,pT,N13);
        } else if(type == 14) {
            prediction = experiments[6] -> GetPrediction(Qmin,Qmax,pT,N14);
        } else if(type == 16) {
            prediction = experiments[7] -> GetPrediction(Qmin,Qmax,pT,N15);
        } else if(type == 17) {
            prediction = experiments[8] -> GetPrediction(Qmin,Qmax,pT,N16);
        } else continue;
#pragma omp critical
        logl += BCMath::LogGaus(prediction,x[4],x[5]);
        if(!first) {
#pragma omp critical
            std::cout << type << " " << prediction << " " << x[4] << " " << x[5] << std::endl;
        }
    }

    if(!first) first = true;

    count++;
    if(count < 20) {
        std::cout << count << "\t" << logl << "\t" << g[0] << ", " << g[1] << ", " << g[2] << "\t" << N4 << "\t" << N5 << "\t" << N10 << "\t" << N11 << std::endl;
    }

    return logl;
}

double NonPertFit::Simpson(const int& nx, const double& dx, const std::vector<double>& f, double& err) {
    double simp, errd, trpz;
    int ms = 0;
    if(dx <= 0) return 0;
    if(nx <= 1) simp = 0;
    else if(nx == 2) {
        simp = (f[0]+f[1])/2.0;
        errd = (f[0]-f[1])/2.0;
    } else {
        ms = nx % 2; 

        double add = 0;
        int nz = nx;
        if(ms == 0) {
            add = (9.0*f[nx-1]+19.0*f[nx-2]-5*f[nx-3]+f[nx-4])/24.0;
            nz--;
        }

        if(nz == 3) {
            simp = (f[0]+4*f[1]+f[2])/3.0;
            trpz = (f[0]+2*f[1]+f[3])/2.0;
        } else {
            double se = f[1];
            double s0 = 0;
            int nm1 = nz - 1;
            for(int i = 3; i < nm1; i = i + 2) {
                int im1 = i - 1;
                se += f[i];
                s0 += f[im1];
            }
            simp = (f[0]+4*se+2*s0+f[nz-1])/3.0;
            trpz = (f[0]+2*se+2*s0+f[nz-1])/2.0;
        }

        errd = trpz - simp;
        simp = simp + add;
    }

    if(fabs(simp) > 1E-10) err = errd/simp;
    else err = 0.0;

    simp *= dx;

    return simp;
}

double NonPertFit::GetValueDSigmaE288(const int& type, const double& Qmin, const double& Qmax, const double& pT) {
    const int NQQ = 10;
    double y = 0;
//    process -> SetBeam(BeamType::pCu);
//    switch(type) {
//        case 12:    
//            process -> SetECM(19.4);
//            y = 0.4;
//            break;
//        case 13:
//            process -> SetECM(23.8);
//            y = 0.21;
//            break;
//        case 14:
//            process -> SetECM(27.4);
//            y = 0.03;
//            break;
//        case 16:
//            process -> SetECM(38.8);
//            y = 0.1;
//            break;
//    }
//
//    double DelQQ = Qmax*Qmax - Qmin*Qmin;
//    double dQQ = DelQQ/(NQQ-1);
//    std::vector<double> dsig;
//    for(int i = 0; i < NQQ; ++i) {
//        double Q = sqrt(Qmin*Qmin+i*dQQ);
//        if(type==16) {
//            double energy = sqrt(pT*pT+Q*Q+y*y);
//            y = 0.5*log((energy+y)/(energy-y));
//        }
//        std::vector<double> result = resbos->GetCalculation()->GetPoint(Q,pT,y);
//        double tmp = result[0]*4.0/9.0+result[2]*1.0/9.0;
//        tmp *= 4*M_PI*M_PI*pow(pyalem(Q*Q),2)/(3.0*3.0*M_PI*Q*Q);
//        dsig.push_back(tmp);
//    }
//
//    double err = 0;
//    double dsigAve = Simpson(NQQ,dQQ,dsig,err)/DelQQ;
//    
//    return dsigAve*DelQQ/2.0/M_PI/pT;
}

double NonPertFit::GetValueDSigmaDPT(const int& type, const double& Qmin, const double& Qmax, const double& pT) {
    return 0;
}

// ---------------------------------------------------------
void NonPertFit::InitResBos() {
    Experiment *CDF1 = new Experiment(ExpType::Collider,"ppb",1800,{0.181,0.167,0.003},{90,91.187,92},Utility::Linspace(0,4,10));
    experiments.push_back(CDF1);
    Experiment *CDF2 = new Experiment(ExpType::Collider,"ppb",1960,{0.181,0.167,0.003},{90,91.187,92},Utility::Linspace(0,4,10));
    experiments.push_back(CDF2);
    Experiment *D01 = new Experiment(ExpType::Collider,"ppb",1800,{0.181,0.167,0.003},{90,91.187,92},Utility::Linspace(0,4,10));
    experiments.push_back(D01);
    Experiment *D02 = new Experiment(ExpType::Collider,"ppb",1960,{0.181,0.167,0.003},{90,91.187,92},Utility::Linspace(0,4,10));
    experiments.push_back(D02);
    Experiment *E288200 = new Experiment(ExpType::Fixed1,"pCu",19.4,{0.181,0.167,0.003},Utility::Linspace(4,8,10),{0.39,0.4,0.41},0,0.4);
    experiments.push_back(E288200);
    Experiment *E288300 = new Experiment(ExpType::Fixed1,"pCu",23.8,{0.181,0.167,0.003},Utility::Linspace(4,12,20),{0.20,0.21,0.22},0,0.21);
    experiments.push_back(E288300);
    Experiment *E288400 = new Experiment(ExpType::Fixed1,"pCu",27.4,{0.181,0.167,0.003},Utility::Linspace(5,14,20),{0.02,0.03,0.04},0,0.03);
    experiments.push_back(E288400);
    Experiment *E605 = new Experiment(ExpType::Fixed1,"pCu",38.8,{0.181,0.167,0.003},Utility::Linspace(7,18,20),{0.09,0.1,0.11},2,0.1);
    experiments.push_back(E605);
    Experiment *R209 = new Experiment(ExpType::Fixed2,"pp",62,{0.181,0.167,0.003},Utility::Linspace(5,11,10),Utility::Linspace(0,2.5,10));
    experiments.push_back(R209);
}

// ---------------------------------------------------------
// double NonPertFit::LogAPrioriProbability(const std::vector<double>& pars)
// {
//     // return the log of the prior probability p(pars)
//     // If you use built-in priors, leave this function commented out.
// }

// ---------------------------------------------------------
// void NonPertFit::CalculateObservables(const std::vector<double>& pars)
// {
//     // Calculate and store obvserables. For example:
//     GetObservable(0) = pow(pars[0], 2);
// }
