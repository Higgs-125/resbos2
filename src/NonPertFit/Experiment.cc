#include "NonPertFit/Experiment.hh"
#include "ResBos/ResBos.hh"
#include "ResBos/Utility.hh"

double Simpson(const int& nx, const double& dx, const std::vector<double>& f, double& err) {
    double simp, errd, trpz;
    int ms = 0;
    if(dx <= 0) return 0;
    if(nx <= 1) simp = 0;
    else if(nx == 2) {
        simp = (f[0]+f[1])/2.0;
        errd = (f[0]-f[1])/2.0;
    } else {
        ms = nx % 2; 

        double add = 0;
        int nz = nx;
        if(ms == 0) {
            add = (9.0*f[nx-1]+19.0*f[nx-2]-5*f[nx-3]+f[nx-4])/24.0;
            nz--;
        }

        if(nz == 3) {
            simp = (f[0]+4*f[1]+f[2])/3.0;
            trpz = (f[0]+2*f[1]+f[3])/2.0;
        } else {
            double se = f[1];
            double s0 = 0;
            int nm1 = nz - 1;
            for(int i = 3; i < nm1; i = i + 2) {
                int im1 = i - 1;
                se += f[i];
                s0 += f[im1];
            }
            simp = (f[0]+4*se+2*s0+f[nz-1])/3.0;
            trpz = (f[0]+2*se+2*s0+f[nz-1])/2.0;
        }

        errd = trpz - simp;
        simp = simp + add;
    }

    if(fabs(simp) > 1E-10) err = errd/simp;
    else err = 0.0;

    simp *= dx;

    return simp;
}

Experiment::Experiment() {
    resbos = new ResBos::ResBos();
}

Experiment::Experiment(ExpType experiment_, std::string beam, double ecm, std::vector<double> g, std::vector<double> qVec, std::vector<double> yVec, int id_, double yPz_) 
    : experiment(experiment_), id(id_), yPz(yPz_) {
    // Setup Settings
    resbos = new ResBos::ResBos();
    IO::Settings settings;
    settings.AddSetting("GF","1.1663787E-5");
    settings.AddSetting("AlphaEMOrder","1");
    settings.AddSetting("AlphaEM0","0.00729735");
    settings.AddSetting("AlphaEMMZ","0.007763340902");
    settings.AddSetting("sinw2","0.2223");
    settings.AddSetting("gamW","2.035");
    settings.AddSetting("gamZ","2.421322");
    settings.AddSetting("gamT","0");
    settings.AddSetting("gamH","0.004");
    settings.AddSetting("AOrder","4");
    settings.AddSetting("BOrder","3");
    settings.AddSetting("COrder","2");
    settings.AddSetting("HOrder","2");
    settings.AddSetting("C1","1");
    settings.AddSetting("C2","1");
    settings.AddSetting("C3","1");
    settings.AddSetting("AsymOrder","1");
    settings.AddSetting("PertOrder","1");
    settings.AddSetting("muR","1");
    settings.AddSetting("muF","1");
    settings.AddSetting("NonPert","BLNY");
    std::string gees = "";
    for(size_t i = 0; i < g.size(); ++i) {
        gees += std::to_string(g[i]);
        if(i != g.size()-1) gees += ", ";
    }
    settings.AddSetting("g",gees);
    settings.AddSetting("Scheme","CSS");
    settings.AddSetting("KinematicCorrection","false");

    // Setup PDFs
    Utility::PDF* pdf = new Utility::PDF("CT18nnlo",0,&settings); 
    std::map<std::thread::id,Utility::PDF*> pdfs;
    pdfs[std::this_thread::get_id()] = pdf;
    Utility::Hoppet* hoppet = new Utility::Hoppet(pdf -> GetPDF(), settings.GetSetting<double>("mc"),
            settings.GetSetting<double>("mb"), settings.GetSetting<double>("mt"));

    // Setup beams
    std::pair<Beam::Beam*, Beam::Beam*> beams = Beam::Beam::MakeBeams(beam,ecm);
    beams.first -> SetPDF(pdf);
    beams.second -> SetPDF(pdf);
    beams.first -> SetHoppet(hoppet);
    beams.second -> SetHoppet(hoppet);
    resbos -> SetBeams(beams);

    // Setup the process
    std::string processName;
    switch(experiment) {
        case ExpType::Collider:
        case ExpType::ColliderShape:
            processName = "Z0";
            break;
        case ExpType::Fixed1:
        case ExpType::Fixed2:
            processName = "A0";
            break;
    }
    std::shared_ptr<ResBos::Process> process = ResBos::ProcessFactory::Instance().Create(
        processName,resbos,settings);
    process -> Initialize(settings);
    resbos -> SetProcess(process);

    // Setup the calculation class
    resum = ResBos::CalculationFactory::Instance().Create("Resummation");
    yTerm = ResBos::CalculationFactory::Instance().Create("YPiece");
    resbos -> SetCalculation(resum);
    resum -> Initialize(&settings,resbos);
    yTerm -> Initialize(&settings,resbos);

    // Setup the convolutions only for the first experiment since the rest can reuse
    static Beam::Convolution *conv;
    if(!conv) {
        std::cout << resum -> GetCOrder() << std::endl;
        conv = new Beam::Convolution(resbos, pdf, hoppet, true,
                resum -> GetCOrder(), resum -> GetScheme(), resum -> GetC1(),
                resum -> GetC2(), resum -> GetC3());
        conv -> SetPDFs(pdfs);
        std::vector<Conv> convEnums = {
            Conv::C1, Conv::C1P1, Conv::C1P1P1, Conv::C1P2, Conv::C2
        };

        for(auto convEnum : convEnums) {
            if(!conv -> LoadGrid(convEnum)) {
                conv -> GenerateGrid(convEnum);
                conv -> SaveGrid(convEnum);
            }
        }

        conv -> GenerateGrid(Conv::C);
    }

    // Add convolution results to the beam class
    beams.first -> SetConvolution(conv);
    beams.second -> SetConvolution(conv);

    // Generate b-space grid
    resum -> SetupBGrid(qVec, yVec);
}

double Experiment::GetPrediction(double QMin, double QMax, double pT, double Norm) {
    double result = 0;
    if(experiment == ExpType::Collider) {
        const double ymax = 4.0;
        const double ymin = 0.0;
        const double DelY = ymax - ymin;
        const double yrange = 10;
        const double dy = DelY/(yrange-1);
        // Only calculate Y Piece if it hasn't been calculated yet
        if(pT > 1.5) {
            auto it = yResults.find(std::make_pair(QMax,pT));
            if(it != yResults.end()) 
                result += it -> second;
            else {
                std::vector<double> SigY(yrange);
                double err;
                for(int i = 0; i < yrange; ++i) {
                    double y = ymin + i*dy;
                    std::vector<double> tmp = yTerm -> GetCalc(QMin,pT,y);
                    SigY[i] = tmp[0];
                }
                yResults[std::make_pair(QMax,pT)] = Simpson(yrange, dy, SigY, err);
                result += yResults[std::make_pair(QMax,pT)];
            }
        }

        std::vector<double> SigW(yrange);
        double err;
        for(int i = 0; i < yrange; ++i) {
            double y = ymin + i*dy;
            std::vector<double> tmp = resum -> GetCalc(QMin,pT,y);
            SigW[i] = tmp[0];
        }
        result += Simpson(yrange, dy, SigW, err);

        return 2.0*result*Norm;
    } else if(experiment == ExpType::Fixed1) {
        const double NQQ = 10;
        const double DelQQ = QMax*QMax - QMin*QMin;
        const double dQQ = DelQQ/(NQQ-1);
        double energy, y;
        // Only calculate Y Piece if it hasn't been calculated yet
        if(pT > 1.5) {
            auto it = yResults.find(std::make_pair(QMax,pT));
            if(it != yResults.end()) 
                result += it -> second;
            else {
                std::vector<double> SigY(NQQ);
                for(int i = 0; i < NQQ; i++) {
                    double Q = sqrt(QMin*QMin+i*dQQ);
                    if(id == 2) {
                        energy = sqrt(pT*pT+Q*Q+yPz*yPz);
                        y = 0.5*log((energy+yPz)/(energy-yPz));
                    } else {
                        y = yPz;
                    }
                    std::vector<double> tmp = yTerm -> GetCalc(Q,pT,y);
                    SigY[i] = tmp[0];
                }
                double err;
                yResults[std::make_pair(QMax,pT)] = Simpson(NQQ,dQQ,SigY,err)/2.0/M_PI/pT;
                result += yResults[std::make_pair(QMax,pT)];
            }
        }
        std::vector<double> SigW(NQQ);
        for(int i = 0; i < NQQ; i++) {
            double Q = sqrt(QMin*QMin+i*dQQ);
            if(id == 2) {
                energy = sqrt(pT*pT+Q*Q+yPz*yPz);
                y = 0.5*log((energy+yPz)/(energy-yPz));
            } else {
                y = yPz;
            }
            std::vector<double> tmp = resum -> GetCalc(Q,pT,y);
            SigW[i] = tmp[0];
        }
        double err;
        return result + Simpson(NQQ,dQQ,SigW,err)/2.0/M_PI/pT;
    } else if(experiment == ExpType::Fixed2) {
        const int Ny = 10;
        const int NQQ = 10;
        const double ymax = 2.5;
        const double ymin = 0.0;
        const double DelY = ymax - ymin;
        const double dy = DelY/(Ny-1);
        const double yrange = Ny;
        const double DelQQ = QMax*QMax-QMin*QMin;
        const double dQQ = DelQQ/(NQQ-1);
        double err;
        // Only calculate Y Piece if it hasn't been calculated yet
        if(pT > 1.5) {
            auto it = yResults.find(std::make_pair(QMax,pT));
            if(it != yResults.end()) 
                result += it -> second;
            else {
                std::vector<double> SigY(NQQ);
                for(int i = 0; i < NQQ; i++) {
                    std::vector<double> SigY1(Ny);
                    double Q = sqrt(QMin*QMin+i*dQQ);
                    for(int j = 0; j < Ny; j++) {
                        double y = ymin + j*dy;
                        std::vector<double> tmp = yTerm -> GetCalc(Q,pT,y);
                        SigY1[j] = tmp[0];
                    }
                    SigY[i] = Simpson(Ny,dy,SigY1,err);
                }
                yResults[std::make_pair(QMax,pT)] = Simpson(NQQ,dQQ,SigY,err)/pT;
                result += yResults[std::make_pair(QMax,pT)];
            }
        }
        std::vector<double> SigW(NQQ);
        for(int i = 0; i < NQQ; i++) {
            std::vector<double> SigW1(Ny);
            double Q = sqrt(QMin*QMin+i*dQQ);
            for(int j = 0; j < Ny; j++) {
                double y = ymin + j*dy;
                std::vector<double> tmp = resum -> GetCalc(Q,pT,y);
                SigW1[j] = tmp[0];
            }
            SigW[i] = Simpson(Ny,dy,SigW1,err);
        }
        return result + Simpson(NQQ,dQQ,SigW,err)/pT;
    } else {
        throw std::runtime_error("Experiment type non-implemented");
    }
}
