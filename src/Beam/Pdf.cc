#include "ResBos/Pdf.hh"
#include "ResBos/Settings.hh"

namespace Utility {

    // Initialize the PDF for the calculation given the input settings
    PDF::PDF(const IO::Settings *s) {
        // Get the name of the PDF from the settings
        pdfName = s->GetSettingString("PDF");
        // Get the set number from the settings
        iSet = s->GetSettingInt("iSet");
        // If it is less than 0, run over central PDF and all error sets, else use only given set
        LHAPDF::PDFSet PDF(pdfName);
        set = PDF;
        if(iSet >= 0) {
            pdf = PDF.mkPDF(iSet);
        } else {
            pdfSet = PDF.mkPDFs();
        }
    }

    // Initialize the PDF for the calculation for a given set and set index
    PDF::PDF(std::string pdfName_, int iSet_, const IO::Settings *s) : pdfName(pdfName_), iSet(iSet_) {
        LHAPDF::PDFSet PDF(pdfName);
        set = PDF;
        pdf = PDF.mkPDF(iSet);
    }

    PDF::PDF(const PDF& other) {
        pdfName = other.pdfName;
        iSet = other.iSet;
        set = other.set;
        pdf = LHAPDF::mkPDF(other.pdf -> lhapdfID());
    }

    // Return the alphas value, ensuring that it is accesssed only on one thread
    double PDF::Alphas(double Q) {
        return pdf -> alphasQ(Q);
    }

    // Return the pdf value, ensuring that it is accesssed only on one thread
    double PDF::Apdf(int pid, double x, double Q) {
        if(x > 1) return 0;
        return pdf->xfxQ2(pid,x,Q*Q)/x;
    }

    // TODO:
    //      - Implement the return of the alphas and pdf value for multiple sets if running in that mode
    //      - Implement the return of a map to all of the parton flavors

    // Return the nf value, ensuring that it is accesssed only on one thread
    int PDF::NF(double Q) {
        const int nf = pdf -> alphaS().numFlavorsQ(Q);
        return nf > 5 ? 5 : nf;
    }

    LHAPDF::PDFUncertainty PDF::Uncertainty(std::vector<double> data, double CL) const {
        return set.uncertainty(data,CL);
    }

}
