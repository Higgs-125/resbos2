include_directories(${RESBOS_SOURCE_DIR}/include)
add_library(ResbosProcess "")
target_link_libraries(ResbosProcess ${RESBOS_PROCESS_LIBRARIES})
target_sources(ResbosProcess
    PRIVATE
    Process.cc
    ProcessFactory.cc
    A0.cc
    DrellYan.cc
    Higgs.cc
    Wpm.cc
    Z0.cc
    )

target_include_directories(ResbosProcess PUBLIC ${RESBOS_SOURCE_DIR}/include)

install(TARGETS ResbosProcess
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib
        RUNTIME DESTINATION bin)
