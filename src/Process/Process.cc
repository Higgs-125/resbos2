#include "ResBos/ResBos.hh"
#include "ResBos/Process.hh"
#include "ResBos/Settings.hh"
#include "ResBos/Enums.hh"
#include "ResBos/QCDConst.hh"
#include "ResBos/Beam.hh"

namespace ResBos {

Process::Process(std::shared_ptr<ResBos> resbos_, const IO::Settings &settings) : resbos(resbos_) {
    ew = std::unique_ptr<Electroweak>(new Electroweak(&settings));
    ps = nullptr;
}

Process::~Process() {}

}
