#include "ResBos/Electroweak.hh"
#include "ResBos/Process.hh"
#include "ResBos/QCDConst.hh"
#include "ResBos/ResBos.hh"
#include "ResBos/Resummation.hh"
#include "ResBos/Settings.hh"
#include "ResBos/ThreadPool.hh"
#include "ResBos/Utility.hh"

#include "ResBos/loguru.hpp"

#ifdef FITTING
#include "ResBos/Convolution.hh"
#include "ResBos/ProgressBar.hh"
#endif


namespace ResBos {
    using namespace std::placeholders;
    REGISTER_CALC(Resummation);

    // Give initial values to bMax and Q0
    double Resummation::bMax = 1.5;
    double Resummation::Q0 = 1.6;

    // Initialize the Resummation class
    void Resummation::Initialize(IO::Settings *s, std::shared_ptr<ResBos> r) {
        gType = GridType::Resum;
        SetAOrder(s->GetSettingInt("AOrder"));
        SetBOrder(s->GetSettingInt("BOrder"));
        SetCOrder(s->GetSettingInt("COrder"));
        SetHOrder(s->GetSettingInt("HOrder"));
        Q0 = s->GetSettingDouble("Q0");
        bMax = s->GetSettingDouble("bMax");
        SetC1(s->GetSettingDouble("C1")*QCD::B0);
        SetC2(s->GetSettingDouble("C2"));
        SetC3(s->GetSettingDouble("C3")*QCD::B0);
        
        std::string gees = s -> GetSettingString("g");
        std::stringstream ss(gees);
        double i;
        while(ss>>i) {
            g.push_back(i);
            if(ss.peek() == ',' || ss.peek() == ' ') ss.ignore();
        }

        std::string nonPertStr = s->GetSettingString("NonPert");
        if(nonPertStr.compare("Gaussian") == 0) iNonPert = NonPertEnum::Gaussian;
        else if(nonPertStr.compare("BLNY") == 0) iNonPert = NonPertEnum::BLNY;
        else if(nonPertStr.compare("SIYY") == 0) iNonPert = NonPertEnum::SIYY;
        else if(nonPertStr.compare("SIYYgy") == 0) iNonPert = NonPertEnum::SIYYgy;
        else if(nonPertStr.compare("None") == 0) iNonPert = NonPertEnum::None;
        else throw std::runtime_error("Process: Invalid Non-pert function choice. Valid options can be found in the manual");
        Calculation::Initialize(s,r);
        DLOG_F(INFO,"Resummation A Order: %d", GetAOrder());
        DLOG_F(INFO,"Resummation B Order: %d", GetBOrder());
        DLOG_F(INFO,"Resummation C Order: %d", GetCOrder());
        if(GetScheme() == Scheme::CFG) {
            DLOG_F(INFO,"Resummation H Order: %d", GetHOrder());
        }
        DLOG_F(INFO,"Resummation NonPert: %s", nonPertStr.c_str());
    }

    // Test function for obtaining b-space
    void Resummation::BSpacePlot(const double Q, const double y) {
        double ecm = resbos -> GetECM();

        double x1, x2;
        x1 = Q/ecm*exp(y);
        x2 = Q/ecm*exp(-y);

        // Ensure that the partonic fractions are in a physically allowed region
        if(x1 > 1 || x2 > 1) return;

        // Fill bins according to benchmarking study
//        std::vector<double> binEdges;
//        for(size_t i = 0; i <= 40; ++i) binEdges.push_back(i);
//        for(size_t i = 0; i < 12; ++i) binEdges.push_back(45+i*5);

        // Fill bins for Ogata study
        std::vector<double> binEdges = Utility::Logspace(-5,2,100);

        // Define couplings using EW class
        double gvu=2*(0.5)-4*2.0/3.0*abs(resbos->GetProcess()->GetEW()->GetSW2());
        double gvd=2*(-0.5)-4*(-1.0/3.0)*abs(resbos->GetProcess()->GetEW()->GetSW2());
        double gau=-1;
        double gad=1.0;
        gvu=gvu/2.0; 
        gvd=gvd/2.0;
        gau=gau/2.0;
        gad=gad/2.0;

        double FAC = sqrt(abs(resbos->GetProcess()->GetEW()->GetZCoupl()));

        double QL_Z1 = (gvu-gau)*FAC/2.0; 
        double QR_Z1 = (gvu+gau)*FAC/2.0;
        double QL_Z2 = (gvd-gad)*FAC/2.0;
        double QR_Z2 = (gvd+gad)*FAC/2.0;

        for(size_t i = 0; i < binEdges.size()-1; ++i) {
            double binCenter = binEdges[i] + (binEdges[i+1]-binEdges[i])/2.0;
            double bspaceZU = FTransform(binCenter, Q, x1, x2, 0);
            double bspaceZD = FTransform(binCenter, Q, x1, x2, 2);

            double bspace = bspaceZU*(QL_Z1*QL_Z1+QR_Z1*QR_Z1)
                          + bspaceZD*(QL_Z2*QL_Z2+QR_Z2*QR_Z2);

            std::cout << binCenter << " " << bspace << std::endl;
        }
    }

    // Driver function to get Resummation piece
    std::vector<double> Resummation::GetCalc(double Q, double qt, double y) {
        std::vector<double> resum(resbos -> GetProcess() -> GetChannels() * resbos -> GetProcess() -> GetResumAngular());

        // Ensure that the rapidity is in a physically allowed region
        if(fabs(y)>GetYMax(Q,qt)) return resum;

        // Calculate the partonic fractions
        const double ecm = resbos -> GetECM();
        double x1, x2;
        if(!KinCorr) {
            x1 = Q/ecm*exp(y);
            x2 = Q/ecm*exp(-y);
        } else {
            x1 = sqrt(Q*Q+qt*qt)/ecm*exp(y);
            x2 = sqrt(Q*Q+qt*qt)/ecm*exp(-y);
        }

        // Ensure that the partonic fractions are in a physically allowed region
        if(x1 > 1 || x2 > 1) return resum;

#ifndef FITTING
        // Perform Hankel Transform using Ogata integration
        auto FTransformFunc = std::function<std::vector<double>(double)>(std::bind(&Resummation::FTransformVec,this,_1,Q,x1,x2));
        std::vector<double> resTot(resum.size());
        auto bRange = std::make_pair(0, 10);
        resTot = ogata.FBT(FTransformFunc, qt, bRange, 1E-8, 1E-8);
        double HardPart = 1.0;
        if(GetScheme() == Scheme::CFG) HardPart = resbos -> H(GetHOrder(),Q,GetC2()*Q);
        const double coupl = resbos -> GetProcess() -> GetCoupling(Q);
        for(size_t resumMode = 0; resumMode < resum.size(); resumMode++) {
            // Obtain the result in terms of pb instead of 1/GeV^2
            resum[resumMode] = resTot[resumMode]*coupl*QCD::hbarc2/pow(ecm,2)*HardPart*qt;
            DLOG_F(INFO,"Resummed(%zu,%f,%f,%f) = %f", resumMode,Q,qt,y,resum[resumMode]);
        }
#else //FITTING
        // Preform integral over the zeros of the bessel function using an accelerated series approach
        auto FTransformFunc = [this,Q,qt,x1,x2](double b){return Resummation::FTransform(b,Q,qt,x1,x2,0);};
        double resTot = ogata.Transform(FTransformFunc,err,1E-6,1E-8)/(2*qt*qt);

        // Calculate the Hard factor if CFG scheme at the scale muR
        double muR = GetC2()*Q;
        double HardPart = 1.0;
        if(GetScheme() == Scheme::CFG) HardPart = resbos -> H(GetHOrder(),muR);
        double coupl = resbos -> GetProcess() -> GetCoupling(Q);

        // Obtain the result in terms of pb instead of 1/GeV^2
        resum[0] = resTot*coupl*QCD::hbarc2/pow(ecm,2)*2*qt*HardPart;
        DLOG_F(INFO,"Resummed(%d,%f,%f,%f) = %f", 0,Q,qt,y,resum[0]);
#endif //FITTING

        return resum;
    }

    std::vector<Conv> Resummation::GetNeededConvs() const{
        std::vector<Conv> neededGrids;
        if(GetCOrder()>0) {
            neededGrids.push_back(Conv::C1);
            if(GetCOrder()>1) {
                neededGrids.push_back(Conv::C2);
            }
        }
        return neededGrids;
    }

//    // Similar to above but for use in Vegas to also do b-space
//    std::vector<double> Resummation::GetResummationVegas(double Q, double qt, double y, double b) {
//        std::vector<double> resum = {0,0,0,0};
//
//        // Ensure that the rapidity is in a physically allowed region
//        if(fabs(y)>process -> GetYMax(Q,qt)) return resum;
//
//        // Calculate the partonic fractions
//        double x1 = Q/process->ECM()*exp(y);
//        double x2 = Q/process->ECM()*exp(-y);
//
//        // Ensure that the partonic fractions are in a physically allowed region
//        if(x1 > 1 || x2 > 1) return resum;
//
//        // Loop over the different resummation Modes, for example symmetric vs asymmetric piece
//        std::vector<double> resTot = FTransformVec(qt*b,Q,qt,x1,x2);
//
//        // Calculate the Hard factor if CFG scheme at the scale muR
//        double muR = C2*Q;
//        double HardPart = 1.0;
//        if(process->GetScheme() == Scheme::CFG) HardPart = process -> H(HOrder,muR);
//        double coupl = process -> GetCoupling(Q);
//
//        // Obtain the result in terms of pb instead of 1/GeV^2
//        for(int i = 0; i < resum.size(); i++) resum[i] = resTot[i]*coupl*QCD::hbarc2/pow(process->ECM(),2)*HardPart;
//
//        return resum;
//    }
//
//    // Similar to above but for the variations
//    std::vector<double> Resummation::GetResummationVegasVar(double Q, double qt, double y, double b,Variation* var) {
//        std::vector<double> resum = {0,0,0,0};
//
//        // Ensure that the rapidity is in a physically allowed region
//        if(fabs(y)>process -> GetYMax(Q,qt)) return resum;
//
//        // Calculate the partonic fractions
//        double x1 = Q/process->ECM()*exp(y);
//        double x2 = Q/process->ECM()*exp(-y);
//
//        // Ensure that the partonic fractions are in a physically allowed region
//        if(x1 > 1 || x2 > 1) return resum;
//
//        // Loop over the different resummation Modes, for example symmetric vs asymmetric piece
//        std::vector<double> resTot = FTransformVecVar(qt*b,Q,qt,x1,x2,var);
//
//        // Calculate the Hard factor if CFG scheme at the scale muR
//        double muR = C2*Q;
//        double HardPart = 1.0;
//        if(process->GetScheme() == Scheme::CFG) HardPart = process -> H(HOrder,muR);
//        double coupl = process -> GetCoupling(Q);
//
//        // Obtain the result in terms of pb instead of 1/GeV^2
//        for(int i = 0; i < resum.size(); i++) resum[i] = resTot[i]*coupl*QCD::hbarc2/pow(process->ECM(),2)*HardPart;
//
//        return resum;
//    }
//
//    // Similar to above but for use in Vegas to also do b-space
//    double Resummation::GetResummationVegasTest(double Q, double qt, double y, double b) {
//        double resum = 0;
//
//        // Ensure that the rapidity is in a physically allowed region
//        if(fabs(y)>process -> GetYMax(Q,qt)) return resum;
//
//        // Calculate the partonic fractions
//        double x1 = Q/process->ECM()*exp(y);
//        double x2 = Q/process->ECM()*exp(-y);
//
//        // Ensure that the partonic fractions are in a physically allowed region
//        if(x1 > 1 || x2 > 1) return resum;
//
//        // Loop over the different resummation Modes, for example symmetric vs asymmetric piece
//        for(int resumMode = 0; resumMode < 1; resumMode++) {
//            double resTot = FTransform(b*qt,Q,qt,x1,x2,resumMode)/qt;
//
//            // Calculate the Hard factor if CFG scheme at the scale muR
//            double muR = C2*Q;
//            double HardPart = 1.0;
//            if(process->GetScheme() == Scheme::CFG) HardPart = process -> H(HOrder,muR);
//            double coupl = process -> GetCoupling(Q);
//
//            // Obtain the result in terms of pb instead of 1/GeV^2
//            resum = resTot*coupl*QCD::hbarc2/pow(process->ECM(),2)*HardPart;
//        }
//
//        return resum;
//    }
//
//    // The integrand to be Fourier Transformed returning a vector
//    std::vector<double> Resummation::FTransformVec(double eta, double Q, double qt, double x1, double x2) {
//        std::vector<double> result = {0,0,0,0};
//        double b = eta/qt;
//        double nonPert = NonPert(b,Q,x1,x2);
//        if(nonPert < 1E-16) return result;
//
//        double sud = Sudakov(b,Q);
//        std::vector<double> cfcf = CxFCxFVec(b,x1,x2);
//        for(int i = 0; i < result.size(); i++) result[i] = eta*sud*cfcf[i]*nonPert*Utility::BesJ0(eta);
//
//        return result;
//    }
//
//    // The integrand to be Fourier Transformed returning a vector for scale or PDF variation
//    std::vector<double> Resummation::FTransformVecVar(double eta, double Q, double qt, double x1, double x2, Variation* var) {
//        std::vector<double> result = {0,0,0,0};
//        double b = eta/qt;
//        double nonPert = NonPert(b,Q,x1,x2);
//        if(nonPert < 1E-16) return result;
//
//        double sud = SudakovVar(b,Q,var);
//        std::vector<double> cfcf = CxFCxFVecVar(b,x1,x2,var);
//        for(int i = 0; i < result.size(); i++) result[i] = eta*sud*cfcf[i]*nonPert*Utility::BesJ0(eta);
//        return result;
//    }

    // The integrand to be Fourier Transformed
    double Resummation::FTransform(const double b, const double Q, 
            const double x1, const double x2, const int mode) const {

        double nonPert = NonPert(b,Q,x1,x2);
        if(nonPert < 1E-16) return 0;

        double mu = GetC3()/GetBStar(b);
// TODO: This is something that needs to be decided on, maybe add a flag to use this option?
//        if(mu < resbos -> GetBeams().first -> GetQMin()) {
//            mu = GetC3()/GetBStar(b,GetC3()/resbos -> GetBeams().first -> GetQMin());
//        }

#ifndef FITTING
        double sud = Sudakov(b,Q);
        if(sud < 1E-8) return 0;
        double cfcf = resbos -> GetProcess() -> CxFCxF(mu,x1,x2,mode);
        return b*sud*cfcf*nonPert;
#else //FITTING
        if(mode != 0) return 0;
        std::vector<double> Point{b,Q,x1,x2};
        auto it = PertTerms.find(Point);
        if(it == PertTerms.end()) {
            double sudcfcf = SudCxFCxFInterp(b,Q,0.5*log(x1/x2));
            PertTerms[Point] = sudcfcf;
            return eta*sudcfcf*nonPert*Utility::BesJ0(eta);
        } else {
            return eta*it->second*nonPert*Utility::BesJ0(eta);
        }
#endif //FITTING
    }

    // The integrand to be Fourier Transformed
    std::vector<double> Resummation::FTransformVec(const double &b, const double &Q, const double &x1, const double &x2) const {
        std::vector<double> result(resbos -> GetProcess() -> GetChannels() * resbos -> GetProcess() -> GetResumAngular(),0);
        const double nonPert = NonPert(b,Q,x1,x2);
        if(nonPert < 1E-16) return result;

        const double mu = GetC3()/GetBStar(b);
// TODO: This is something that needs to be decided on, maybe add a flag to use this option?
//        if(mu < resbos -> GetBeams().first -> GetQMin()) {
//            mu = GetC3()/GetBStar(b,GetC3()/resbos -> GetBeams().first -> GetQMin());
//        }

        const double sud = Sudakov(b,Q);
        if(sud < 1E-8) return result;
        for(size_t i = 0; i < result.size(); ++i) {
            const double cfcf = resbos -> GetProcess() -> CxFCxF(mu,x1,x2,i);
            result[i] = b*sud*cfcf*nonPert;
        }
        return result;
    }

    // Calculation of Non-Pert functions. To add a new function, make sure to also add it to Enums.hh
    double Resummation::NonPert(const double &b, const double &Q, 
            const double &x1, const double &x2) const {
        double b2 = b*b;
        static constexpr double x0 = 0.01, lambda = 0.2;
        switch(iNonPert) {
            case NonPertEnum::Gaussian:
                return exp(-b2*g[0]);
            case NonPertEnum::BLNY:
                return exp(-b2*(g[0]+g[2]*log(100*x1*x2)+g[1]*log(Q/2.0/Q0)));
            case NonPertEnum::SIYY:
                return exp(-(b2*(g[0]+g[2]*(pow(x0/x1,lambda)+pow(x0/x2,lambda)))+g[1]*log(b/GetBStar(b))*log(Q/Q0)));
            case NonPertEnum::SIYYgy:
                return exp(-b2*(g[0]+g[2]*(pow(x0/x1,lambda)+pow(x0/x2,lambda))+g[1]*log(Q/2.0/Q0)));
            case NonPertEnum::None:
                return 1.0;
            default:
                throw std::runtime_error("Resummation: Invalid NonPert Function");
        }
        return 0;
    }

    // Calculation of the perturbative Sudakov factor
    double Resummation::Sudakov(const double &b, const double &Q) const {
        const double bStar = GetBStar(b);

        const double uplim = 2*log(GetC2()*Q);
        const double dnlim = 2*log(GetC1()/bStar);

        auto SudFunc = std::function<double(double)>(std::bind(&Resummation::SudInt,this,_1,Q));
        Utility::Integrate SudIntegral(SudFunc);
        if(uplim > dnlim) return exp(-SudIntegral.DEIntegrate(dnlim,uplim,1E-6,1E-6));
        else return exp(SudIntegral.DEIntegrate(uplim,dnlim,1E-6,1E-6));
    }

//    // Calculation of the perturbative Sudakov factor
//    double Resummation::SudakovVar(double b, double Q, Variation* var) {
//        double bStar = GetBStar(b);
//
//        double uplim = 2*log(var -> GetC2()*Q);
//        double dnlim = 2*log(var -> GetC1()/bStar);
//
//        auto SudFunc = std::function<double(double)>(std::bind(&Resummation::SudIntVar,this,_1,Q,var));
//        Utility::Integrate SudIntegral(SudFunc);
//        if(uplim > dnlim) return exp(-SudIntegral.Adzint(dnlim,uplim,1E-8,1E-4,2,2));
//        else return exp(SudIntegral.Adzint(uplim,dnlim,1E-16,1E-8,2,2));
//    }

    // Calculation of the perturbative Sudakov integrand
    double Resummation::SudInt(const double &logMu2, const double &Q) const {
        const double mu = sqrt(exp(logMu2));
        const double alpi = resbos -> GetAlpi(mu);
        const int nf = resbos -> GetNF(mu);

        const double sQ = GetC2()*Q;
        const double dlQ = log(sQ*sQ)-logMu2;
        return GetA(nf,alpi,GetC1())*dlQ+GetB(nf,alpi,GetC1(),GetC2());
    }

//    // Calculation of the perturbative Sudakov integrand
//    double Resummation::SudIntVar(double logMu2, double Q, Variation *var) {
//        double mu = sqrt(exp(logMu2));
//        double alpi = process -> pdf -> Alpi(mu);
//        int nf = process -> pdf -> NF(mu);
//
//        double sQ = var -> GetC2()*Q;
//        double dlQ = log(sQ*sQ)-logMu2;
//        return GetA(nf,alpi,var -> GetC1())*dlQ+GetB(nf,alpi,var -> GetC1(), var -> GetC2());
//    }

    double Resummation::GetA(const int nf, const double alpi, const double C1) const {
        if(GetAOrder()==1) return alpi*GetA1();
        if(GetAOrder()==2) return alpi*(GetA1()+alpi*GetA2(nf,C1));
        if(GetAOrder()==3) return alpi*(GetA1()+alpi*(GetA2(nf,C1)+alpi*GetA3(nf,C1)));
        if(GetAOrder()==4) return alpi*(GetA1()+alpi*(GetA2(nf,C1)+alpi*(GetA3(nf,C1)+alpi*GetA4(nf,C1))));
        if(GetAOrder()==5) 
            return alpi*(GetA1()+alpi*(GetA2(nf,C1)+alpi*(GetA3(nf,C1)
                            +alpi*(GetA4(nf,C1)+alpi*GetA5(nf,C1)))));
        return 0;
    }

    double Resummation::GetB(const int nf, const double alpi, const double C1,
            const double C2) const {
        if(GetBOrder()==1) return alpi*GetB1(nf,C1,C2);
        if(GetBOrder()==2) return alpi*(GetB1(nf,C1,C2)+alpi*GetB2(nf,C1,C2));
        if(GetBOrder()==3) return alpi*(GetB1(nf,C1,C2)+alpi*(GetB2(nf,C1,C2)+alpi*GetB3(nf,C1,C2)));
        return 0;
    }

    // Asymptotic calculation for the Fourier Transform
    double Resummation::FTAsym(double b, double x1, double x2, double s, double qt, int mode) const {
        double dbe = 1.0/(b*qt*qt);

        double fic0 = FIC(b,x1,x2,s,qt,mode);
        double fic1 = (FIC(b+dbe/2.0,x1,x2,s,qt,mode)-FIC(b-dbe/2.0,x1,x2,s,qt,mode))/dbe;
        double fic2 = (FIC(b+dbe,x1,x2,s,qt,mode)-2.0*FIC(b,x1,x2,s,qt,mode)+FIC(b-dbe,x1,x2,s,qt,mode))/dbe/dbe;

        double fis1 = (FIS(b+dbe/2.0,x1,x2,s,qt,mode)-FIS(b-dbe/2.0,x1,x2,s,qt,mode))/dbe;

        double t1 = b*qt-M_PI/4.0;
        double t2 = cos(t1);
        double t3 = sin(t1);
        double t4 = 1.0/qt;

        double yic = t2*(-t4*t4*fic1)+t3*(-t4*fic0+t4*t4*t4*fic2);
        double yis = t3*(-t4*t4*fis1)+t2*(t4*fic0-t4*t4*t4*fic2);

        return 0.5*(yic-yis);
    }

    // Helper function for the asymptotic calculation
    std::vector<double> Resummation::FICIS(double b, double x1, double x2, double s, double qt, int mode) const {
        std::vector<double> results;
        results.resize(3);

        results[0] = 1.0/(b*qt);
        results[1] = sqrt(2*b/M_PI/qt);
        #ifndef FITTING
        results[2] = NonPert(b,sqrt(s),x1,x2)*Sudakov(b,sqrt(s))*resbos -> GetProcess() -> CxFCxF(GetC3()/GetBStar(b),x1,x2,mode);
        #else
        double y = 0.5*log(x1/x2);
        results[2] = NonPert(b,sqrt(s),x1,x2)*SudCxFCxFInterp(b,sqrt(s),y);
        #endif
        
        return results;
    }

    // Helper function for the asymptotic calculation
    double Resummation::FIC(double b, double x1, double x2, double s, double qt, int mode) const {
        std::vector<double> results = FICIS(b,x1,x2,s,qt,mode);
        double t3 = p00+p02*pow(results[0],2)+p04*pow(results[0],4);
        return results[1]*t3*results[2];
    }

    // Helper function for the asymptotic calculation
    double Resummation::FIS(double b, double x1, double x2, double s, double qt, int mode) const {
        std::vector<double> results =FICIS(b,x1,x2,s,qt,mode);
        double t3 = q01*results[0]+q03*pow(results[0],3);
        return results[1]*t3*results[2];
    }

    // Fitting functions
    #ifdef FITTING
    void Resummation::SetupBGrid(std::vector<double> qVec, std::vector<double> yVec) {
        std::vector<double> bVec = Utility::Logspace(-5,2.2,100);
        GenerateBGrid(bVec, qVec, yVec); 
    }

    void Resummation::GenerateBGrid(std::vector<double> bVec, std::vector<double> qVec, std::vector<double> yVec) {
        std::cout << "Generating b-space grid..." << std::endl;
        sudCxFCxFGrid = new Utility::Grid3D(bVec,qVec,yVec);

        ProgressBar pb(bVec.size()*qVec.size()*yVec.size());

        for(auto b : bVec) {
            for(auto q : qVec) {
                for(auto y : yVec) {
                    double sud = Sudakov(b,q);
                    double x1 = q/resbos->GetECM()*exp(y);
                    double x2 = q/resbos->GetECM()*exp(-y);
                    double bStar = GetBStar(b);
                    double mu = GetC3()/bStar;
                    if(mu < resbos -> GetBeams().first -> GetQMin()) {
                        mu = GetC3()/GetBStar(b,GetC3()/resbos -> GetBeams().first -> GetQMin());
                    }
                    sudCxFCxFGrid->AddPoint(b,q,y,sud*resbos -> GetProcess() -> CxFCxF(mu,x1,x2,0));
                    pb.Update(1);
                    pb.Display();
                }
            }
        }
        pb.Done();
        std::cout << "Finished generating b-space grid." << std::endl;
    }

    double Resummation::FTransformFit(double eta, double Q, double qt, double x1, double x2) {
        double b = eta/qt;
        double nonPert = NonPert(b,Q,x1,x2);
        if(nonPert < 1E-16) return 0;

        std::vector<double> Point{b,Q,x1,x2};
        auto it = PertTerms.find(Point);
        if(it == PertTerms.end()) {
            double y = 0.5*log(x1/x2); 
            double sudcfcf = SudCxFCxFInterp(b,Q,y);
            PertTerms[Point] = sudcfcf;
            return eta*sudcfcf*nonPert*Utility::BesJ0(eta);
        } else {
            return eta*it->second*nonPert*Utility::BesJ0(eta);
        }
    }

    double Resummation::SudCxFCxFInterp(double b, double Q, double y) {
        double sudcfcf = sudCxFCxFGrid -> Interpolate(b,Q,y);
        if(sudcfcf != sudcfcf || b < 1E-5) {
            double ecm = resbos -> GetECM();
            double x1 = Q/ecm*exp(y);
            double x2 = Q/ecm*exp(-y);
            double mu = GetC3()/GetBStar(b);
            if(mu < resbos -> GetBeams().first -> GetQMin()) {
                mu = GetC3()/GetBStar(b,GetC3()/resbos -> GetBeams().first -> GetQMin());
            }
            sudcfcf = Sudakov(b,Q)*resbos->GetProcess()->CxFCxF(mu,x1,x2,0);
        }
        return sudcfcf;
    }


    #endif
}

