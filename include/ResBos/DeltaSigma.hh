#ifndef DELTASIGMA_HH
#define DELTASIGMA_HH

#include <vector>
#include <map>

#include "ResBos/Calculation.hh"
#include "ResBos/Enums.hh"

namespace IO {
    class Settings;
}

namespace Beam {
    enum class BDF;
}

typedef std::map<Beam::BDF,double> ConvMap;
namespace ResBos {
    class ResBos;

    class DeltaSigma : public Calculation {
        public:
            // Initialize Asymptotic class
            static std::unique_ptr<Calculation> Create() {
                return std::unique_ptr<DeltaSigma>(new DeltaSigma());
            }
            void Initialize(IO::Settings*,std::shared_ptr<ResBos>);

            static std::string GetName() {return "DeltaSigma";}
            std::vector<Conv> GetNeededConvs() const;
            bool IsRegistered() {return registered;}

            // Calculate Asymptotic piece
            virtual std::vector<double> GetCalc(double,double,double);
//            virtual Grid3D GridGen();

        private:
            DeltaSigma() {};
            static bool registered;

            virtual std::vector<double> CalcTerms(double,double,double,double) const; 

            // alpha_s DeltaSigma functions
            double V12(int,ConvMap,ConvMap) const;
            double V11(int,ConvMap,ConvMap) const;
            double V10(int,ConvMap,ConvMap) const;
            
            // alpha_s^2 DeltaSigma functions
            double V24(int,ConvMap,ConvMap) const;
            double V23(int,ConvMap,ConvMap) const;
            double V22(int,ConvMap,ConvMap) const;
            double V21(int,ConvMap,ConvMap) const;
            double V20(int,ConvMap,ConvMap) const;

            // alpha_s^3 DeltaSigma functions
            double V36(int,ConvMap,ConvMap) const;
            double V35(int,ConvMap,ConvMap) const;
            double V34(int,ConvMap,ConvMap) const;
            double V33(int,ConvMap,ConvMap) const;
            double V32(int,ConvMap,ConvMap) const;
            double V31(int,double,ConvMap,ConvMap) const;
            double V30(int,double,ConvMap,ConvMap) const;

            // Scale Dependent functions
            //   alpha_s terms
            double V10log(int,ConvMap,ConvMap,double,double) const;

            //   alpha_s^2 terms
            double V22log(int,ConvMap,ConvMap,double,double) const;
            double V21log(int,ConvMap,ConvMap,double,double) const;
            double V20log(int,ConvMap,ConvMap,double,double) const;
    };
}

#endif
