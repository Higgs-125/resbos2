#ifndef GRID2D_HH
#define GRID2D_HH

#include <vector>
#include <algorithm>

namespace Utility {

    class Grid2D {
        public:
            Grid2D(std::vector<double> x, std::vector<double> y) :
                x_{x}, y_{y} {
                    xSize = x.size(); ySize = y.size();
                    grid.reserve(x.size()*y.size());
                };

            Grid2D(std::vector<double> x, std::vector<double> y, std::vector<double> g) :
                x_(x), y_(y), grid(g) {
                    xSize = x.size(); ySize = y.size();
                };

            virtual ~Grid2D() {}

            virtual void AddPoint(unsigned int i, unsigned int j, double value);
            virtual void AddPoint(double x, double y, double value);

            // Find... Finds the nearest point to the value input
            virtual std::vector<double>::const_iterator FindX(double x) const; 
            virtual std::vector<double>::const_iterator FindY(double y) const; 
            virtual double FindPoint(unsigned int i, unsigned int j) const;
            virtual double FindPoint(double x, double y) const;
            virtual unsigned int FindPointLocation(double x, double y) const;

            //Get returns point at a given grid point
            virtual double GetX(int i) const {return x_[i];}
            virtual double GetY(int i) const {return y_[i];}
            virtual unsigned int GetXDim() const {return x_.size();}
            virtual unsigned int GetYDim() const {return y_.size();}

            // Preforms a 2D interpolation of the grid to find the result at the desired input point
            virtual double Interpolate(double x, double y, int xOrder=5, int yOrder=4);
            //        double Polint(std::vector<double> x_, std::vector<double> y_, int n, double x) const;

            void PrintGrid();
        private:
            std::vector<double> x_, y_, grid;
            unsigned int xSize, ySize;
    };

}


#endif
