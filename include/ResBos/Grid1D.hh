#ifndef GRID1D_HH
#define GRID1D_HH

#include <vector>
#include <algorithm>

namespace Utility {

    class Grid1D {
        public:
            Grid1D() {}
            Grid1D(std::vector<double> x) :
                x_{x} {
                    xSize = x.size(); 
                    grid.reserve(x.size());
                };

            Grid1D(std::vector<double> x, std::vector<double> g) :
                x_(x), grid(g) {
                    xSize = x.size();
                };

            virtual ~Grid1D() {}

            virtual void AddPoint(unsigned int i, double value);
            virtual void AddPoint(double x, double value);

            // Find... Finds the nearest point to the value input
            virtual std::vector<double>::const_iterator FindX(double x) const; 
            virtual double FindPoint(unsigned int i) const;
            virtual double FindPoint(double x) const;
            virtual unsigned int FindPointLocation(double x) const;

            //Get returns point at a given grid point
            virtual double GetX(int i) const {return x_[i];}
            virtual unsigned int GetXDim() const {return x_.size();}

            // Preforms a 1D interpolation of the grid to find the result at the desired input point
            virtual double Interpolate(double x, int xOrder=4) const;

            void PrintGrid() const;
        private:
            std::vector<double> x_, grid;
            unsigned int xSize;
    };

}


#endif

