#ifndef GRID3D_HH
#define GRID3D_HH

#include <vector>
#include <algorithm>

namespace Utility {

    class Grid3D {
        public:
            Grid3D(std::vector<double> x, std::vector<double> y, std::vector<double> z) :
                x_(x), y_(y), z_(z) {
                    xSize = x.size(); ySize = y.size(); zSize = z.size();
                    grid.reserve(x.size()*y.size()*z.size());
                };

            Grid3D(std::vector<double> x, std::vector<double> y, std::vector<double> z,std::vector<double> g) :
                x_(x), y_(y), z_(z), grid(g) {
                    xSize = x.size(); ySize = y.size(); zSize = z.size();
                };

            virtual ~Grid3D() {}

            virtual void AddPoint(unsigned int i, unsigned int j, unsigned int k, double value);
            virtual void AddPoint(double x, double y, double z, double value);

            // Find... Finds the nearest point to the value input
            virtual std::vector<double>::const_iterator FindX(double x) const; 
            virtual std::vector<double>::const_iterator FindY(double y) const; 
            virtual std::vector<double>::const_iterator FindZ(double z) const; 
            virtual double FindPoint(unsigned int i, unsigned int j, unsigned int k) const;
            virtual double FindPoint(double x, double y, double z) const;
            virtual unsigned int FindPointLocation(double x, double y, double z) const;

            //Get returns point at a given grid point
            virtual double GetX(int i) const {return x_[i];}
            virtual double GetY(int i) const {return y_[i];}
            virtual double GetZ(int i) const {return z_[i];}
            virtual unsigned int GetXDim() const {return x_.size();}
            virtual unsigned int GetYDim() const {return y_.size();}
            virtual unsigned int GetZDim() const {return z_.size();}

            // Preforms a 3D interpolation of the grid to find the result at the desired input point
            virtual double Interpolate(double x, double y, double z, int xOrder=4, int yOrder=4, int zOrder=4);
            //        double Polint(std::vector<double> x_, std::vector<double> y_, int n, double x) const;

            void PrintGrid();
        private:
            std::vector<double> x_, y_, z_, grid;
            unsigned int xSize, ySize, zSize;
    };

}


#endif

