#ifndef PHASESPACE_HH
#define PHASESPACE_HH

#include <vector>
#include <map>
#include <iostream>

#include "ResBos/Random.hh"

class FourVector;

class PhaseSpace {
    public:
        // Constructors and Destructors
        PhaseSpace();
        PhaseSpace(const double& S, const int& iMonte=2, const int& iQt=0, const int& nPrimaryPart=2);
        virtual ~PhaseSpace() {
            delete rand;
        }

        // Phase Space generators
        virtual double PhaseGen(std::vector<double> x, std::vector<FourVector> &p);
        virtual double PhaseSpace3(std::vector<double>  x, double &lx1, double &lx2, std::vector<FourVector> &p);
        virtual double x2Phase(double*, double, double, double, FourVector&, FourVector&) const;
        virtual double Wresph(std::vector<double>,double,std::vector<double>&,
                std::vector<FourVector>&);
//        std::vector<double> GetPSPoint() { return phaseSpace; };
//        std::vector<FourVector> GetMomentum() { return p;};

        //Setters
        virtual void SetECM(double ECM) {S = ECM*ECM;}
        virtual void SetNPrimaryPart(int nPrimaryPart_) {nPrimaryPart = nPrimaryPart_;}
        virtual void SetNSecondaryPart(int nSecondaryPart_) {nSecondaryPart = nSecondaryPart_;}
        virtual void SetIMonte(int iMonte_) {iMonte = iMonte_;}
        virtual void SetIQt(int iQt_) {iQt = iQt_;}
        virtual void SetPhiDep(bool phiDep_) {phiDep = phiDep_;}
        virtual void SetMass(std::vector<double> Mass) {for(size_t i = 0; i < Mass.size(); i++) this -> Mass.push_back(Mass[i]);}
        virtual void SetQRange(double Qmin, double Qmax) { qMin = Qmin; qMax = Qmax;}
        virtual void SetQtRange(double Qtmin, double Qtmax) { qtMin = Qtmin; qtMax = Qtmax;}
        virtual void SetRandomGenerator(Random* random) {rand = random;}

    private:
        double YMaximum(double q, double qT);
        double FillMomenta3(std::array<double,5>  r, std::vector<FourVector> &p);
        FourVector GetYVect(const double dQ, const FourVector Q, const FourVector X, const FourVector Z) const;
        void AdditionalDecays() {return;}

        double S, qMin, qMax, qtMin, qtMax;
        int iMonte, iQt, nPrimaryPart, nSecondaryPart;
//        std::vector<FourVector> p;
        std::vector<double> Mass;//, phaseSpace;
        bool phiDep;
        Random *rand;

};

#endif
