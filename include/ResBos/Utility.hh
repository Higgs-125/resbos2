#ifndef UTILITY_H
#define UTILITY_H

#include <iostream>
#include <cmath>
#include <functional>
#include <map>
#include <vector>
#include <array>
#include <algorithm>
#include <iterator>

#include "ResBos/BesselZeros.hh"

#define minLevel 10
#define maxLevel 100

const double inf = std::numeric_limits<double>::infinity();

static const long N = 50;
const double bernoulli[N] = {
    1, -0.5               , 0.16666666666666666     , 0,
    -0.03333333333333333  , 0, 0.023809523809523808 , 0,
    -0.03333333333333333  , 0, 0.07575757575757576  , 0,
    -0.2531135531135531   , 0, 1.1666666666666667   , 0,
    -7.092156862745098    , 0, 54.971177944862156   , 0,
    -529.1242424242424    , 0, 6192.123188405797    , 0,
    -86580.25311355312    , 0, 1.4255171666666667e6 , 0,
    -2.7298231067816094e7 , 0, 6.015808739006424e8  , 0,
    -1.5116315767092157e10, 0, 4.296146430611667e11 , 0,
    -1.3711655205088334e13, 0, 4.883323189735932e14 , 0,
    -1.9296579341940068e16, 0, 8.416930475736827e17 , 0,
    -4.0338071854059454e19, 0, 2.1150748638081993e21, 0,
    -1.2086626522296526e23, 0  
};
const double fac_inv[N] = {
    1                     , 0.5                   , 0.16666666666666666   ,
    0.041666666666666664  , 0.008333333333333333  , 0.001388888888888889  ,
    0.0001984126984126984 , 0.0000248015873015873 , 2.7557319223985893e-6 ,
    2.755731922398589e-7  , 2.505210838544172e-8  , 2.08767569878681e-9   ,
    1.6059043836821613e-10, 1.1470745597729725e-11, 7.647163731819816e-13 ,
    4.779477332387385e-14 , 2.8114572543455206e-15, 1.5619206968586225e-16,
    8.22063524662433e-18  , 4.110317623312165e-19 , 1.9572941063391263e-20,
    8.896791392450574e-22 , 3.8681701706306835e-23, 1.6117375710961184e-24,
    6.446950284384474e-26 , 2.4795962632247972e-27, 9.183689863795546e-29 ,
    3.279889237069838e-30 , 1.1309962886447718e-31, 3.7699876288159054e-33,
    1.2161250415535181e-34, 3.800390754854744e-36 , 1.151633562077195e-37 ,
    3.387157535521162e-39 , 9.67759295863189e-41  , 2.688220266286636e-42 ,
    7.265460179153071e-44 , 1.911963205040282e-45 , 4.902469756513544e-47 ,
    1.225617439128386e-48 , 2.9893108271424046e-50, 7.117406731291439e-52 ,
    1.6552108677421951e-53, 3.7618428812322616e-55, 8.359650847182804e-57 ,
    1.817315401561479e-58 , 3.866628513960594e-60 , 8.055476070751238e-62 ,
    1.643974708316579e-63 , 3.2879494166331584e-65
};

namespace Utility {

    template<unsigned... Is> struct seq{};
    template<unsigned N, unsigned... Is>
        struct gen_seq : gen_seq<N-1, N-1, Is...>{};
    template<unsigned... Is>
        struct gen_seq<0, Is...> : seq<Is...>{};

    const int _phases = 6;
    constexpr int _size = 6*pow(2,_phases);

    struct DETable {
        static constexpr int length = _size;

        double abscissa[length];
        double weights[length]; 
    };

    constexpr double t2(const int k) {
        return exp(k*std::ldexp(1,-_phases));
    }

    constexpr double u1(const int k) {
        return 0.5*M_PI/2.0*(t2(k)+1.0/t2(k));
    }

    constexpr double t3(const int k) {
        return exp(0.5*M_PI/2.0*(t2(k)-1.0/t2(k)));   
    }

    constexpr double t4(const int k) {
        return 0.5*(t3(k)+1.0/t3(k));   
    }

    constexpr double get_abcs(const int k) {
        return 1.0/(t3(k)*t4(k));
    }

    constexpr double get_weight(const int k) {
        return u1(k)/(t4(k)*t4(k));
    }

    template<unsigned... Is>
        constexpr DETable TableGen(seq<Is...>){
            return {{ get_abcs(Is)... }, {get_weight(Is)... }};
        }

    constexpr DETable TableGen(){
        return TableGen(gen_seq<_size>{});
    }

    // Table for Double Exponential integration
    static constexpr DETable table = TableGen();

    class Integrate {
        public:
            Integrate();
            Integrate(std::function<double(double)>);
            Integrate(std::vector<std::function<double(double)>>);
            //            virtual double Integral(double a, double b, double t);
            virtual void SetFunction(std::function<double(double)> func) {f = func;}
            virtual void SetFunctionVec(std::vector<std::function<double(double)>> funcVec) {fVec = funcVec;}
            virtual double AdaptiveIntegrate(double a, double b, double aerr, double rerr,int mode);
            virtual double GaussKronrodInt(double a, double b, double &err);
            virtual double NewtonCotesEPInt(double a, double b, double &err);
            //            virtual double TrapezoidNEInt(double a, double b, double &err);
            virtual double NewtonCotesInt(double a, double b, double &err);
            virtual double DEQuad(double a, double b, double &err);
            virtual double Adzint(const double, const double, const double, const double, int, int);
            virtual double DEIntegrate(const double, const double, const double, const double);
            virtual double OscIntegrate(std::vector<double>, const double);
            virtual std::vector<double> DEIntegrateVec(const double, const double, const double, const double);
            virtual std::vector<double> OscIntegrateVec(std::vector<double>, const double);
            void ScaleFunc(int n);

        private:
            void sglint(int iact, double f1, double f2, double f3, double dx, double *fint, double *ester);
            void Privcal(int i, double &result, double &err);
            void Privspl(int i);
            double GetFunctionValue(double x);
            double CoarseIntegrate(double a, double b);
            double FineIntegrate(double a, double b);
            int count;
            std::function<double(double)> f;
            std::map<double, double> functionGrid;
            static const int maxint = 5000;
            std::array<double,maxint> err, u, v, result, fu, fv, fw;
            double fa, fb;
            int ib, iacta, iactb, ier, scaleFac;
            int numint;
            double res, ers;

            // Variables for vector integrators
            size_t size; 
            std::vector<std::function<double(double)>> fVec;
            std::vector<double> GetFunctionValueVec(double x);

            // Variables to determine which adaptive routine to use
            std::vector<std::function<double(double,double,double&)>> Integrators;

            // Variables for Gauss-Kronrod Integration
            const int n = 7, maxSubintervals = 100;
            const std::vector<double> KonrodWgts = {0.022935322010529,0.063092092629979,
                0.104790010322250,0.140653259715525,0.169004726639267,0.190350578064785,
                0.204432940075298,0.209482141084728};
            const std::vector<double> GaussWgts = {0.129484966168870,0.279705391489277,
                0.381830050505119,0.417959183673469};
            const std::vector<double> absc = {0.991455371120813,0.949107912342759,
                0.864864423359769,0.741531185599394,0.586087235467691,0.405845151377397,
                0.207784955007898};
            std::vector<double> error,lowerList,upperList,integralList;
    };

    class OgataQuad {
        public:
            OgataQuad();
            OgataQuad(double _h) : h(_h) {init();}
            double Transform(std::function<double(const double&)>, double&, const double& rerr = 1E-16, const double& aerr = 1E-16) const;
            std::vector<double> Transform(std::function<std::vector<double>(const double&)> func, std::vector<double>& err, const double& rerr, const double& aerr) const;
            void SetStep(double _h);
            double GetParams(std::function<double(const double&)>, double start=0.05, const double& decrement = 2, const double& rtol = 1e-3, const double& atol = 1e-8, const size_t& maxiter = 15);

        private:
            void init();
            double h;
            std::vector<double> xf, weights;
            size_t N;
    };

    class OgataQuad2 {
        public:
            using func = std::function<double(const double&)>;
            using funcVec = std::function<std::vector<double>(const double&)>;

            OgataQuad2(const int& _nu=0) : nu(_nu) { init(); }

            // Single value integrators
            double OgataT(const func&, const double&, const double& rerr = 1E-16,
                          const double& aerr = 1E-16) const noexcept;
            double OgataU(const func&, const double&, const double& rerr = 1E-16,
                          const double& aerr = 1E-16) const noexcept;
            double FBTU(const func&, const double&, const std::pair<double, double>&,
                        const double& rerr = 1E-16, const double& aerr = 1E-16) const noexcept;
            double FBT(const func&, const double&, const std::pair<double, double>&,
                       const double& rerr = 1E-16, const double& aerr = 1E-16) const noexcept;

            // Vector integrators
            std::vector<double> OgataT(const funcVec&, const double&, const double& rerr= 1E-16,
                                       const double& aerr = 1E-16) const noexcept;
            std::vector<double> FBT(const funcVec&, const double&, const std::pair<double, double>&,
                                    const double& rerr = 1E-16, const double& aerr = 1E-16) const noexcept;

        private:
            void init();
            double GetHu(const funcVec&, const double&, const std::pair<double, double>&) const noexcept;
            double GetHu(const func&, const double&, const std::pair<double, double>&) const noexcept;
            double GetHt(const double&) const noexcept;
            static inline bool AbsCompare(const double& a, const double& b) noexcept {
                return (std::abs(a) < std::abs(b));
            }
            static inline double GetPsi(const double& t) {
                return t*tanh(M_PI/2*sinh(t));
            }
            static inline double GetPsiP(const double& t) {
                return M_PI*t*(-pow(tanh(M_PI*sinh(t)/2),2) + 1)*cosh(t)/2
                       + tanh(M_PI*sinh(t)/2);
            }
            int nu;
            static constexpr size_t maxN = 50000;
            static constexpr std::array<double, maxN> jnZeros = besselZero;
            std::array<double, maxN> xi, w;
            static constexpr double hDefault = 1e-2;
    };

    inline double BesJ0(double x){return j0(x);}
    inline double BesJ1(double x){return j1(x);}
    inline double BesY0(double x){return y0(x);}
//    double DivDif(double* f, double* a, int n, double x, int m); 
    double zeta(double n);
    double xLi(long n, const double x);
    double Polint(std::vector<double> x_, std::vector<double> y_, int n, double x);
//    double LambertW(double z);
    inline void PrintProgBar(int percent) {
        static std::string bar;
        static double percentOld;
        for(int i = 0; i < 50; i++) {
            if(i < percentOld/2) continue;
            else if(i < percent/2) bar.replace(i,1,"=");
            else if(i == percent/2) bar.replace(i,1,">");
            else bar.replace(i,1," ");
        }
        percentOld = percent;

        std::cout << "\r [" << bar << "] ";
        std::cout.width(3);
        std::cout << percent << "%    " << std::flush;
    }

    template<typename T>
    class LogspaceGen {
    private:
        T curValue, base;
    
    public:
        LogspaceGen(T first, T base) : curValue(first), base(base) {}
    
        T operator()() {
            T retval = curValue;
            curValue *= base;
            return retval;
        }
    };

    inline std::vector<double> Logspace(double start, double stop, int num = 50, double base = 10) {
        double realStart = pow(base, start);
        double realBase = pow(base, (stop-start)/(num-1));

        std::vector<double> retval;
        retval.reserve(num);
        std::generate_n(std::back_inserter(retval), num, Utility::LogspaceGen<double>(realStart,realBase));
        return retval;
    } 

    template<typename T>
    class LinspaceGen {
    private:
        T curValue, base;
    
    public:
        LinspaceGen(T first, T base) : curValue(first), base(base) {}
    
        T operator()() {
            T retval = curValue;
            curValue += base;
            return retval;
        }
    };

    inline std::vector<double> Linspace(double start, double stop, int num = 50) {
        double step = (stop-start)/(num-1);

        std::vector<double> retval;
        retval.reserve(num);
        std::generate_n(std::back_inserter(retval), num, Utility::LinspaceGen<double>(start,step));
        return retval;
    } 


}

#endif
