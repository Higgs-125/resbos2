#ifndef CONVOLUTION_HH
#define CONVOLUTION_HH

#include <map>
#include <vector>
#include <thread>
#include <functional>

#include "ResBos/Enums.hh"
#include "ResBos/QCDConst.hh"

//#include "fftwpp/Array.h"
//#include "fftwpp/Complex.h"
//#include "fftwpp/convolution.h"

const extern std::thread::id mainThread;

namespace Utility {
    class Grid2D;
    class PDF;
    class Hoppet;
}

namespace ResBos {
    class ResBos;
}

namespace Beam {
    typedef std::map<int,Utility::Grid2D*> ConvGrid;
    typedef std::map<std::thread::id,Utility::PDF*> PDFThreads;

    // Class to preform the convolutions and store grids
    class Convolution {
        public:
            // Constructors and Destructors
            Convolution() {}; // Purely for testing purposes
            Convolution(std::shared_ptr<ResBos::ResBos>, Utility::PDF*, 
                    Utility::Hoppet*,
                    const bool gridGen=true, const int COrder=2, const Scheme=Scheme::CFG, 
                    const double C1=QCD::B0, const double C2=1, const double C3=QCD::B0);
            Convolution(const Convolution&);
            virtual ~Convolution();

            // Setters
            void SetPDFs(const PDFThreads pdfs_) {pdfs = pdfs_; pdf = pdfs[mainThread];}
            void SetHoppet(Utility::Hoppet* hoppet_) {hoppet = hoppet_;}
            void SetGridGen(const bool gridGen_) {gridGen = gridGen_;}
            void SetOrder(const int COrder_) {COrder = COrder_;}
            void SetScheme(const Scheme scheme_) {scheme = scheme_;}
            void SetC1C2C3(const double C1_, const double C2_, const double C3_) {
                C1 = C1_*QCD::B0; C2 = C2_; C3 = C3_*QCD::B0;
            }

            // Getters
            virtual double GetConv(const int, const double, const double, const Conv);
            virtual std::map<int,double> GetConv(const double, const double, const Conv);
            virtual ConvGrid GetGrid(const Conv) const;

            // Generate Grid
            void GenerateGrid(PDFThreads,Utility::Hoppet*,Conv);
            void GenerateGrid(Conv conv);

            // Search for grids
            bool GetC1Grid();
            bool GetC1P1Grid();
            bool GetC1P1P1Grid();
            bool GetC1P2Grid();
            bool GetC2Grid();
            bool GetC2P1Grid();
            bool GetG1Grid();
            bool GetG1P1Grid();

            // Convolution I/O
            bool SaveGrid(const Conv) const;
            bool LoadGrid(const Conv);

            // Testing FFT
            void CalcC1GridFFTTest(const int mode = 0);
            void CalcC2GridFFTTest(const int mode = 0);

        private:
            // Calculate the grids
            void CalcCGrid();
            std::map<int,Utility::Grid2D*> CalcC1Grid(const int mode = 0);
            std::map<int,Utility::Grid2D*> CalcC2Grid(const int mode = 0);
            std::map<int,Utility::Grid2D*> CalcG1Grid(const int mode = 0);

            // Helper functions for the FFT calculation
            double C1qq(const double&) const;
            double C1qg(const double&) const;
            double C1gq(const double&) const;

            double C2qq(const double&) const;
            double C2qqb(const double&) const;
            double C2qqp(const double&) const;
            double C2qg(const double&) const;
    
            // Actual convolution calculations
            double CConv(int, double, double, std::thread::id id=mainThread);
            double C1Conv(int, double, double, int mode=0, std::thread::id id=mainThread);
            double C2Conv(int, double, double, int mode=0, std::thread::id id=mainThread);
            double G1Conv(int, double, double, int mode=0, std::thread::id id=mainThread);
            double Convolve(double, std::function<double(double)>) const;

            // Functions for CSS scheme dependence, must be run each time since grids will
            // be stored in CFG format to make more universal
            double SchemeDependence(int,double,double,Conv) const;
            double HCxF(int,double,double,double,int) const;

            // Integrands and plus functions for convolution calculations
            double C1xF(int,double,double,double,int,std::thread::id id=mainThread) const;
            double C2xF(int,double,double,double,int,std::thread::id id=mainThread);
            double SxF(int,double,double,double,int,std::thread::id id=mainThread) const;
            double G1xF(int,double,double,double,int,std::thread::id id=mainThread) const;

            // Scale variation of the C functions
            double ScaleVariation(int,int,double,double);

            // Variables
            bool gridGen;
            int COrder;
            std::shared_ptr<ResBos::ResBos> resbos;
            Utility::PDF *pdf;
            ConvGrid Cf, C1f, C2f, C1P1f, C1P2f, C1P1P1f, C2P1f, G1f, G1P1f;
            std::vector<double> xVec, qVec;
            Utility::Hoppet *hoppet;
            std::map<std::thread::id, Utility::PDF*> pdfs;
            double muF, muR;
            Scheme scheme;
            double C1, C2, C3;

            // FFT Convolution variables
            // static constexpr unsigned int nQ = 108;
            static constexpr unsigned int nQ = 108;
            // static constexpr unsigned int N = pow(2,8);
            static constexpr unsigned int N = 150;
            static constexpr double qBase = 0.226236;
            std::map<int,std::vector<double>> C2qqVals;
            std::map<double,double> CQQMap, CQGMap, CQQBMap, CQQPMap;
            std::map<double,double> CGGMap, CGQMap;
            std::vector<double> C2qgVals, C2qqbVals, C2qqpVals;

    };
}

#endif
