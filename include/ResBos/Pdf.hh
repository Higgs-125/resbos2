#ifndef PDF_H
#define PDF_H

#include <cmath>
#include <mutex>

// Prevent warnings from LHAPDF popping up, 
// since we have no control over their code
#pragma GCC diagnostic push 
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wignored-qualifiers"
#include "LHAPDF/LHAPDF.h"
#include "LHAPDF/AlphaS.h"
#pragma GCC diagnostic pop


namespace IO {
    class Settings;
}

namespace Utility {

    // Class for interfacing to LHAPDF, and ensuring thread safety
    class PDF{
        public:
            PDF() {pdf = NULL;}
            PDF(const PDF&);
            PDF(const IO::Settings *s);
            PDF(std::string pdfName, int iSet, const IO::Settings *s);
            virtual ~PDF() {
                delete pdf;
                for(auto pdfi : pdfSet) {
                    if(pdfi != nullptr) delete pdfi; 
                }
            }

            virtual double Alphas(double Q);
            virtual double Alpi(double Q) { return Alphas(Q)/M_PI; }
            virtual double Alpi2(double Q) { return as.alphasQ(Q)/M_PI; }
            virtual double Apdf(int pid, double x, double Q);
            virtual int NF(double Q);
            LHAPDF::PDF* GetPDF() const { return pdf; }
            LHAPDF::PDF* GetPDF(int i) const { return pdfSet[i]; }
            virtual int nPDFs() const { return set.size(); }
            LHAPDF::PDFUncertainty Uncertainty(std::vector<double>,double) const;
            virtual double qMin() const { return pdf -> qMin(); }
            virtual double qMax() const { return pdf -> qMax(); }
            virtual double xMin() const { return pdf -> xMin(); }
            virtual double xMax() const { return pdf -> xMax(); }
            LHAPDF::PDFInfo info() const { return pdf -> info(); }

            virtual int GetISet() const { return iSet; }
            virtual std::string GetName() const { return pdfName; }

        private:
            std::string pdfName;
            int iSet;
            LHAPDF::PDF* pdf;
            LHAPDF::PDFSet set;
            std::vector<LHAPDF::PDF*> pdfSet;
            LHAPDF::AlphaS_ODE as;
            std::mutex alphasMutex, pdfMutex, nfMutex;
    };
}

#endif
