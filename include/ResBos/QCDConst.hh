#ifndef QCDCONST_HH
#define QCDCONST_HH

#include <cmath>

namespace QCD {
    constexpr double CA = 3.0;
    constexpr double CF = (CA*CA-1)/(2*CA);
    constexpr double beta0(const int nf) {return (11*CA-2*nf)/12.0;}
    constexpr double beta1(const int nf) {return (17*CA*CA-5*CA*nf-3*CF*nf)/24.0;};
    const double ZETA3 = 1.2020569031595942854;
    const double ZETA5 = 1.0369277551433699263;
    const double pi2 = M_PI*M_PI;
    const double hbarc2 = 0.38937966E9;
    const double EulerGamma = 0.5772156649015328606;
    const double B0 = 2*exp(-EulerGamma);
}

#endif
