#ifndef PROGRESS_BAR_HH
#define PROGRESS_BAR_HH

#include <chrono>

class ProgressBar {
    public:
        ProgressBar() = delete;
        ProgressBar(int);
        virtual ~ProgressBar() {};
        void Update(int);
        void Display();
        void Done();

    private:
        static constexpr double width = 50.0;
        int nTasks, nTasksRemaining, nTasksComplete;
        double GetETA();
        double timeLeft, lastTimeLeft;
        std::chrono::system_clock::time_point start, current;
};

#endif
