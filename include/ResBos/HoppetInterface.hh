#ifndef HOPPETINTERFACE_HH
#define HOPPETINTERFACE_HH

#include <mutex>

// Prevent warnings from LHAPDF popping up, 
// since we have no control over their code
#pragma GCC diagnostic push 
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wignored-qualifiers"
#include "LHAPDF/LHAPDF.h"
#pragma GCC diagnostic pop

#include "ResBos/Enums.hh"

// Header file to contain the interface between the C++ portion of the code, and the fortran of HOPPET

namespace Utility {

    class Hoppet{
        public:
            Hoppet() {};
            Hoppet(LHAPDF::PDF *p);
            virtual ~Hoppet(); 

            virtual double GetConvolution(int,double,double,int, int j = 0, int k = 0);
            virtual double GetConvolution(int,double,double,Splitting);
            virtual std::map<int,double> GetConvolution(double,double,int,int j=0,int k=0);
            void SwitchSet(LHAPDF::PDF *p);
        private:
            static std::mutex hoppetMutex;
    };

}

#endif
