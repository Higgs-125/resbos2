#ifndef CALCULATION_HH
#define CALCULATION_HH

#include <vector>
#include <map>
#include <functional> 
#include <mutex>
#include <cmath>
#include <unordered_map>
#include <memory>

#include "ResBos/Beam.hh"
#include "ResBos/Enums.hh"
#include "ResBos/QCDConst.hh"

namespace IO {
    class Settings;
}

namespace Utility {
    class Grid3D;
    class Grid2D;
    class PDF;
    class Hoppet;
}

class PhaseSpace;

namespace ResBos {
    typedef std::map<int,Utility::Grid3D*> MapGrid3D;
    class ResBos;
    
    class Calculation {
        public:
            // Initialize the ResBos class for preforming the calculations
            Calculation() {
                resbos = NULL;
            }
            virtual ~Calculation();
            virtual void Initialize(IO::Settings*, std::shared_ptr<ResBos>);
            virtual bool IsRegistered() = 0;

            // Getters
            virtual inline int GetAsymOrder() const {return AsymOrder;}
            virtual inline int GetPertOrder() const {return PertOrder;}
            virtual inline int GetDeltaSigmaOrder() const {return DeltaSigmaOrder;}
            virtual inline int GetAOrder() const {return AOrder;}
            virtual inline int GetBOrder() const {return BOrder;}
            virtual inline int GetCOrder() const {return COrder;}
            virtual inline int GetHOrder() const {return HOrder;}

            virtual inline double GetA1() const {return A1;}
            virtual inline double GetA2(int nf, double C1_ = QCD::B0) const {
                if(C1_ == QCD::B0) return A2[nf-3];
                return A2[nf-3]-2*QCD::beta0(nf)*A1*log(QCD::B0/C1_);
            }
            virtual inline double GetA3(int nf, double C1_ = QCD::B0) const {
                if(C1_ == QCD::B0) return A3[nf-3];
                return A3[nf-3]+pow(2*QCD::beta0(nf)*log(QCD::B0/C1_),2)*A1-2*log(QCD::B0/C1_)*(QCD::beta1(nf)*A1+2*QCD::beta0(nf)*A2[nf-3]);
            }
            virtual inline double GetA4(int nf, double /*C1_ = 1.0/QCD::B0*/) const {return A4[nf-3];}
            virtual inline double GetA5(int nf, double /*C1_ = 1.0/QCD::B0*/) const {return A5[nf-3];}

            virtual inline double GetB1(int nf, double C1_ = QCD::B0, double C2_ = 1.0) const {
                if(C1_ == QCD::B0 && C2_ == 1.0) return B1[nf-3];
                return B1[nf-3]-2*A1*log(QCD::B0*C2_/C1_);
            }
            virtual inline double GetB2(int nf, double C1_ = QCD::B0, double C2_ = 1.0) const {
                if(C1_ == QCD::B0 && C2_ == 1.0) return B2[nf-3];
                return B2[nf-3]-2*A2[nf-3]*log(QCD::B0*C2_/C1_)+QCD::beta0(nf)*(2*A1*pow(log(QCD::B0/C1_),2)-2*A1*pow(log(C2_),2)+2*B1[nf-3]*log(C2_));
            }
            virtual inline double GetB3(int nf, double C1_ = QCD::B0, double C2_ = 1.0) const {
                if(C1_ == QCD::B0 && C2_ == 1.0) return B3[nf-3];
                return B3[nf-3]-2*A3[nf-3]*log(QCD::B0*C2_/C1_)+2*QCD::beta1(nf)*(A1*pow(log(QCD::B0/C1_),2)+log(C2_)*(B1[nf-3]-A1*log(C2_)))
                    -4.0/3.0*pow(QCD::beta0(nf),2)*(2*A1*pow(log(QCD::B0/C1_),3)+pow(log(C2_),2)*(2*A1*log(C2_)-3*B1[nf-3]))
                    +4.0*QCD::beta0(nf)*(A2[nf-3]*pow(log(QCD::B0/C1_),2)+log(C2_)*(B2[nf-3]-A2[nf-3]*log(C2_)));
            }

            virtual inline double GetC1() const {return C1;}
            virtual inline double GetC2() const {return C2;}
            virtual inline double GetC3() const {return C3;}
            virtual inline double GetMuR() const {return muR;}
            virtual inline double GetMuF() const {return muF;}

            virtual inline Scheme GetScheme() const {return scheme;}
            virtual std::vector<Conv> GetNeededConvs() const = 0;
            virtual std::vector<double> GetPoint(double,double,double);
//            virtual std::pair<Beam::BDFMap,Beam::BDFMap> CalcMap(int, int, double, double, double) const;
//            virtual double GetConvolution(int hadron, int pid, double x, double Q, Conv c) const;

            // Setters
            virtual inline void SetAsymOrder(int order) {AsymOrder = order;}
            virtual inline void SetPertOrder(int order) {PertOrder = order;}
            virtual inline void SetDeltaSigmaOrder(int order) {DeltaSigmaOrder = order;}
            virtual inline void SetAOrder(int AOrder_) {AOrder = AOrder_;}
            virtual inline void SetBOrder(int BOrder_) {BOrder = BOrder_;}
            virtual inline void SetCOrder(int COrder_) {COrder = COrder_;}
            virtual inline void SetHOrder(int HOrder_) {HOrder = HOrder_;}
            virtual inline void SetC1(double C1_) {C1 = C1_;}
            virtual inline void SetC2(double C2_) {C2 = C2_;}
            virtual inline void SetC3(double C3_) {C3 = C3_;}
            virtual inline void SetMuR(double muR_) {muR = muR_;}
            virtual inline void SetMuF(double muF_) {muF = muF_;}
            virtual inline void SetResumScales(double C1, double C2, double C3) {
                SetC1(C1); SetC2(C2); SetC3(C3);
            }
            virtual inline void SetScales(double C1, double C2, double C3, double muR, double muF) {
                SetResumScales(C1,C2,C3); SetMuR(muR); SetMuF(muF);
            }
            virtual void SetScheme(Scheme s);
            virtual void SetNonPertCoefficients(std::vector<double>) {}      

            // Other
            virtual double GetYMax(double,double);
            virtual double GetYMax(double);
            virtual double GetQTMax(double,double);
            //virtual std::function<double(double*,double)> GetXSect();
            //virtual double XSect(double*,double);
            virtual std::vector<double> GetCalc(double,double,double) = 0;
            virtual void SetupBGrid(std::vector<double>, std::vector<double>) {return;}
            virtual void BSpacePlot(double,double) {
                throw std::runtime_error("Calculation: This is only valid for the resummed piece");
            }
            virtual double FTransform(const double,const double,const double,const double,const double,const int) const {
                throw std::runtime_error("Calculation: This is only valid for the resummed piece");
            }

            // Grid Functions
            void GridSetup(const IO::Settings&);
            virtual void GridGen();
            virtual void MergeGrid() {};

            // Grid I/O
            virtual bool SaveGrid();
            virtual bool LoadGrid();

        protected:
            CentralScale scale;
            //double H(const int, const double);
            std::vector<double> qVals, qTVals, yVals;
            GridType gType; 
            MapGrid3D grid;
            std::shared_ptr<ResBos> resbos;
            bool KinCorr;
            double qMin, qMax, qTMin, qTMax, yMin, yMax;

        private:
            A5Choice A5Err;
            Scheme scheme;
            int AsymOrder, PertOrder, DeltaSigmaOrder;
            double C1, C2, C3, muR, muF;
            int AOrder, BOrder, COrder, HOrder;
            double A1;
            std::array<double,3> A2, A3, A4, A5;
            std::array<double,3> B1, B2, B3;
    };

    class CalculationFactory {
        public:
            using TCreateMethod = std::unique_ptr<Calculation>(*)();
            TCreateMethod CreateFunc;

            static CalculationFactory& Instance();

            bool Register(const std::string, TCreateMethod);
            std::unique_ptr<Calculation> Create(const std::string&);

        private:
            CalculationFactory() : methods() {};
            std::map<std::string, TCreateMethod> methods;
    };

#define REGISTER_CALC(calculation) \
    bool calculation::registered = CalculationFactory::Instance().Register(calculation::GetName(), \
            calculation::Create);

}

#endif
