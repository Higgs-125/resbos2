#ifndef WMA_HH
#define WMA_HH 

#include <vector>
#include <cmath>
#include <functional>

#include "ResBos/Resummation.hh"
#include "ResBos/Asymptotic.hh"
#include "ResBos/Enums.hh"

namespace IO {
    class Settings;
}

namespace Utility {
    class Grid2D;
}

namespace ResBos {
    typedef std::map<int,Utility::Grid3D*> MapGrid3D;
    typedef std::map<int,Utility::Grid2D*> MapGrid2D;
    class ResBos;

    class WmA : public Resummation, public Asymptotic {
        public:
            static std::unique_ptr<Calculation> Create() {
                return std::unique_ptr<WmA>(new WmA());
            }
            void Initialize(IO::Settings*,std::shared_ptr<ResBos>);

            static std::string GetName() {return "WmA";}
            bool IsRegistered() {return registered;}

            double CalcCrossing(int, double, double);
            double GetCalcCrossing(double,double,double) const;
            virtual std::vector<double> GetCalc(double,double,double);
            std::vector<Conv> GetNeededConvs() const;
            std::vector<double> GetPoint(double,double,double);
            double GetPointMode(double,double,double,int);

            void GridGen();

            bool LoadGrid();
            bool SaveGrid();
            void MergeGrid();

        protected:
            WmA() {};
            static bool registered;

        private:
            virtual std::vector<double> GetResummation(double,double,double);
            virtual std::vector<double> GetAsymptotic(double,double,double);
            bool needResum, needAsym;
            MapGrid3D gridResum, gridAsym;
            MapGrid2D gridCrossing;
    };
}

#endif
