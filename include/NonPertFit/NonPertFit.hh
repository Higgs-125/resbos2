// ***************************************************************
// This file was created using the bat-project script.
// bat-project is part of Bayesian Analysis Toolkit (BAT).
// BAT can be downloaded from http://mpp.mpg.de/bat
// ***************************************************************

#ifndef __BAT__NONPERTFIT__H
#define __BAT__NONPERTFIT__H

#include <BAT/BCModel.h>

#include <memory>
#include <string>
#include <vector>

#include <TH1D.h>

#include "NonPertFit/Experiment.hh"

// Include stuff needed for calculation
namespace ResBos {
    class ResBos;
}

// This is a NonPertFit header file.
// Model source code is located in file NonPert/NonPertFit.cxx

// ---------------------------------------------------------
class NonPertFit : public BCModel
{

public:

    // Constructor
    NonPertFit(const std::string& name);

    // Destructor
    ~NonPertFit();

    // Overload LogLikelihood to implement model
    double LogLikelihood(const std::vector<double>& pars);

    // Overload LogAprioriProbability if not using built-in 1D priors
    // double LogAPrioriProbability(const std::vector<double> & pars);

    // Overload CalculateObservables if using observables
    void CalculateObservables(const std::vector<double> & pars) {};
    
    // ResBos calculation tools
    void InitResBos();

    // Tools for calculating LogLikelihood
    double Simpson(const int& nx, const double& dx, const std::vector<double>& f, double& err);
    double GetValueDSigmaE288(const int&, const double&, const double&, const double&);
    double GetValueDSigmaDPT(const int&, const double&, const double&, const double&);

    // Vector to hold experiments
    std::vector<Experiment*> experiments;

private:
    const double N0err = 0.003;
    const double N1err = 0.03;
    const double N2err = 0.03;
    const double N3err = 0.028;
    const double N4err = 0.04;
    const double N5err = 0.04;
    const double N6err = 0.03;
    const double N7err = 0.04;
    const double N8err = 0.04;
    const double N9err = 0.04;
    const double N10err = 0.04;
    const double N11err = 0.04;
    const double N12err = 0.25;
    const double N13err = 0.25;
    const double N14err = 0.25;
    const double N15err = 0.15;
    const double N16err = 0.10;
    int count;
};
// ---------------------------------------------------------

#endif
