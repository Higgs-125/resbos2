#ifndef EXPERIMENT_HH
#define EXPERIMENT_HH

#include "ResBos/Beam.hh"
#include "ResBos/Calculation.hh"
#include "ResBos/Convolution.hh"
#include "ResBos/Enums.hh"
#include "ResBos/Grid3D.hh"
#include "ResBos/HoppetInterface.hh"
#include "ResBos/Process.hh"
#include "ResBos/Pdf.hh"
#include "ResBos/ResBos.hh"
#include "ResBos/Settings.hh"
#include "ResBos/Utility.hh"
#include "ResBos/Vegas.hh"

#include <algorithm>


enum class ExpType {Collider, ColliderShape, Fixed1, Fixed2};

namespace ResBos {
    class ResBos;
    class Process;
}

class Experiment {
    public:
        Experiment();
        Experiment(ExpType,std::string,double,std::vector<double>, std::vector<double>, std::vector<double>, int id_ = 0, double yPz = 0);
        virtual ~Experiment() {
            delete resbos; 
        };
        double GetPrediction(double,double,double,double);
        void SetGees(std::vector<double> g) {resum -> SetNonPertCoefficients(g);}

    private:
        ExpType experiment;
        ResBos::ResBos* resbos;
        std::shared_ptr<ResBos::Calculation> resum, yTerm;
        std::map<std::pair<double,double>,double> yResults;
        int id;
        double yPz;

};

#endif
